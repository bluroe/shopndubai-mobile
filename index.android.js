/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict';

import React, { AppRegistry } from 'react-native';
import TelyBazaar from './app';

AppRegistry.registerComponent('TelyBazaar', () => TelyBazaar);
