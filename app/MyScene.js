import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  Image,
  Dimensions,
} from 'react-native';
import * as locationActions from './actions/location';
import images from './config/images';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  }
});

class MyScene extends Component {

  static propTypes = {
    text: PropTypes.string.isRequired
  }

  componentDidMount() {
    // this.props.actions.getAndSetCurrentLocation();
  }

  render() {
    const width = Dimensions.get('window').width;
    return (
      <View style={styles.container}>

        <Image source={images.bg} style={{width,height:250}} />
        <Text>Fooz</Text>
        <Text>{Dimensions.get('window').width}</Text>
        <Text>{this.props.text}</Text>
        <TouchableHighlight onPress={() => alert('whoohoo')} style={{padding: 10}}>
          <Text>Whoohoo</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

function select(store) {
  return {
    geolocation: store.location.geolocation
  }
}

function actions(dispatch) {
  return {
    actions: bindActionCreators(locationActions, dispatch)
  }
}

export default connect(select, actions)(MyScene);
