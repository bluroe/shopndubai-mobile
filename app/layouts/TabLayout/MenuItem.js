import React from 'react';
import {
  TouchableHighlight,
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';

class MenuItem extends React.Component {

  render() {
    var icon = this.props.selected ? this.props.selectedIcon : this.props.icon;
    return (
      <TouchableHighlight onPress={this.props.onPress} style={styles.container} underlayColor="#ccc">
        <View style={styles.container}>
          <Image style={styles.icon} source={icon} />
          <Text style={styles.title}>
            {this.props.title}
          </Text>
        </View>
      </TouchableHighlight>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    height: 50,
    alignItems: 'center',
  },
  icon: {
    marginRight: 10
  },
  title: {
    fontFamily: 'cabin'
  }
})

export default MenuItem;
