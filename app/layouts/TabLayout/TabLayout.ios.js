import React, { Component } from 'react';
import {
  TabBarIOS,
  TabBarItemIOS,
  View,
  Text,
  NavigatorIOS,
  Navigator
} from 'react-native';
import { connect } from 'react-redux';
import { switchTab } from '../../actions/navigation';
import Home from '../../routes/Home';
import Category from '../../routes/Category';
import ProductListing from '../../routes/ProductListing';
import ProductView from '../../routes/ProductView';
import Search from '../../routes/Search';
import Cart from '../../routes/Cart';
import DeliveryAddress from '../../routes/DeliveryAddress';
import OrderConfirmation from '../../routes/OrderConfirmation';
import OrderPlaced from '../../routes/OrderPlaced';
import Orders from '../../routes/Orders';
import OrderView from '../../routes/OrderView';
import images from '../../config/images';
import Services from '../../routes/Services';

class TabLayout extends Component {

  constructor() {
    super();
    this.state = {
      selectedTab: 'blueTab'
    }
  }

  onTabSelect(tab) {
    if(this.props.tab !== tab) {
      this.props.dispatch(switchTab(tab));
    }
  }


  render() {
    const { cartItems } = this.props;
    return (
      <TabBarIOS>
        <TabBarIOS.Item
          title="Home"
          ref="home"
          selected={this.props.tab === 'home'}
          onPress={this.onTabSelect.bind(this, 'home')}
          icon={images.home}>
          <Navigator
            initialRoute={{}}
            renderScene={this.props.renderHomeScene}
          />
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Cart"
          ref="cart"
          badge={cartItems == 0 ? null : cartItems}
          selected={this.props.tab === 'cart'}
          onPress={this.onTabSelect.bind(this, 'cart')}
          icon={images.cart}>
          <Navigator
            initialRoute={{}}
            renderScene={this.props.renderCartScene}
          />
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Services"
          ref="services"
          selected={this.props.tab === 'services'}
          onPress={this.onTabSelect.bind(this, 'services')}
          icon={images.briefcase}>
          <Services />
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Orders"
          ref="orders"
          selected={this.props.tab === 'orders'}
          onPress={this.onTabSelect.bind(this, 'orders')}
          icon={images.list}>
          <Navigator
            initialRoute={{}}
            renderScene={this.props.renderOrderScene}
          />
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  }

  getNavigator() {
    return this.refs[this.props.tab];
  }

}

const select = (store) => ({
  tab: store.navigation.tab,
  cartItems: store.cart.items.length
})

export default connect(select, null, null, {withRef: true})(TabLayout);
