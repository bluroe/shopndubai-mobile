import React from 'react';
import {
  ToolbarAndroid,
  View,
  Text,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  StyleSheet,
  Navigator
} from 'react-native';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { switchTab } from '../../actions/navigation';
// import StyleSheet from '../../common/AppStyleSheet';
import MenuItem from './MenuItem';
import DrawerLayout from './DrawerLayout';
import Home from '../../routes/Home';
import Cart from '../../routes/Cart';
import Orders from '../../routes/Orders';
import images from '../../config/images';
import Services from '../../routes/Services';

class TabLayout extends React.Component {

  static childContextTypes = {
    openDrawer: React.PropTypes.func
  }

  constructor() {
    super();
    this.openDrawer = this.openDrawer.bind(this);
    this.navigationView = this.navigationView.bind(this);
  }

  getChildContext() {
    return {
      openDrawer: this.openDrawer
    }
  }

  openDrawer() {
    this.refs.drawer.openDrawer();
  }

  onTabSelect(tab) {
    if(this.props.tab !== tab) {
      this.props.dispatch(switchTab(tab));
    }
    this.refs.drawer.closeDrawer();
  }

  navigationView() {
    return (
      <View style={{flex: 1,backgroundColor: '#fff'}}>
        <View style={{height:150,backgroundColor:'#555',justifyContent:'center'}}>
          <View style={{backgroundColor:'#fff',borderRadius:50,width:100,marginLeft:20,alignItems:'center'}}>
            <Image source={images.telyicon} style={{width:60}} resizeMode="contain" />
          </View>
        </View>
        <MenuItem
          title="Home"
          onPress={this.onTabSelect.bind(this, 'home')}
          icon={images.home}
        />
        <MenuItem
          title="Cart"
          onPress={this.onTabSelect.bind(this, 'cart')}
          icon={images.cart}
        />
        <MenuItem
          title="Services"
          onPress={this.onTabSelect.bind(this, 'services')}
          icon={images.briefcase}
        />
        <MenuItem
          title="Orders"
          onPress={this.onTabSelect.bind(this, 'orders')}
          icon={images.list}
        />
      </View>
    )
  }

  onActionSelected() {
    alert('fooz');
  }

  renderContent(setNavRef) {
    switch(this.props.tab) {
      case 'home':
        return (
          <Navigator
            initialRoute={{}}
            key="home"
            ref={setNavRef}
            renderScene={this.props.renderHomeScene}
          />
        );
      case 'cart':
        return (
          <Navigator
            initialRoute={{}}
            key="cart"
            ref={setNavRef}
            renderScene={this.props.renderCartScene}
          />
        )
      case 'orders':
        return (
          <Navigator
            initialRoute={{}}
            key="orders"
            ref={setNavRef}
            renderScene={this.props.renderOrderScene}
          />
        )
      case 'services':
        return <Services />;
      default:
        return null
    }
  }

  render() {
    return (
      <DrawerLayout
        drawerWidth={290}
        ref="drawer"
        drawerPosition="left"
        renderNavigationView={this.navigationView}>
        {this.renderContent(nav => this.navigator = nav)}
      </DrawerLayout>
    );
  }

  getNavigator() {
    return this.navigator;
  }

}

const styles = StyleSheet.create({
  button: {
    padding: 10,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

const select = (store) => ({
  tab: store.navigation.tab
})

export default connect(select, null, null, {withRef: true})(TabLayout);
