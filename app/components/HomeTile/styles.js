import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    height: 150
  },
  image: {
    height:150,
    width:null
  },
  overlay: {
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.25)',
    width,
    position: 'absolute',
    left: 0,
    top: 0,
  },
  title: {
    color: '#fff',
    fontSize: 30,
    fontFamily: 'cabin',
    textAlign: 'center'
  }
})
