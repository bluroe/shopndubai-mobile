import React from 'react';
import { View, Image, TouchableHighlight, Text } from 'react-native';
import styles from './styles';
import images from '../../config/images';
import settings from '../../config/settings';

const HomeTile = (props) => {
  const { category, onPress } = props;
  const { images } = category;

  const firstImage = images.length ? {uri: settings.serverURL + images[0].filename} : images.noimage;

  return (
    <TouchableHighlight onPress={onPress}>
      <View style={styles.container}>
        <Image
          resizeMode="cover"
          style={styles.image}
          source={firstImage}
        />
        <View style={styles.overlay}>
          <Text style={styles.title}>{category.name.toUpperCase()}</Text>
        </View>
      </View>
    </TouchableHighlight>
  );

}

HomeTile.propTypes = {
  category: React.PropTypes.object,
  onPress: React.PropTypes.func,
}

HomeTile.defaultProps = {
  image: images.fish,
  onPress: () => console.log('button clicked')
}

export default HomeTile;
