import React from 'react';
import { TouchableHighlight, Text, View } from 'react-native';
import styles from './styles';

const Button = (props) => {

  const { text, onPress } = props;

  const extraStyles = props.style ? props.style : {};

  return (
    <TouchableHighlight onPress={onPress} style={[styles.button, extraStyles]} underlayColor="#e25a3f">
      <Text style={styles.text}>{text}</Text>
    </TouchableHighlight>
  );

}

Button.propTypes = {
  onPress: React.PropTypes.func,
  text: React.PropTypes.string
};

Button.defaultProps = {
  text: 'Button Text',
  onPress: () => console.log('Sample Button')
}

export default Button;
