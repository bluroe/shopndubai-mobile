import { create } from '../../common/AppStyleSheet';
import { colors } from '../../config/styles';

export default create({
  button: {
    height: 36,
    backgroundColor: colors.primaryBtnBg,
    alignItems: 'center',
    justifyContent: 'center',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowColor: '#000',
    elevation: 2,
    ios: {
      borderRadius: 18,
      width: 250,
    },
    android: {
      borderRadius: 2,
      flexDirection: 'row',
      flexGrow: 1,
      margin: 10,
      paddingHorizontal:40
    }
  },
  text: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'cabin',
    backgroundColor: 'transparent'
  }
});
