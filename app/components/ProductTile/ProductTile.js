import React from 'react';
import Image from 'react-native-image-progress';
import { View, Text, TouchableOpacity, TouchableHighlight } from 'react-native';
import styles from './styles';
import images from '../../config/images';
import settings from '../../config/settings';

const ProductTile = (props) => {

  const { add, product, onPress } = props;

  let image;
  if(product.images.length) {
    var firstImage = product.images[0].filename;
    image = {uri: firstImage.startsWith('/') ? settings.serverURL + firstImage : firstImage};
  } else {
    image = images.noimage;
  }

  const { variants } = product;
  let pricing = variants[0].price;
  if(variants.length > 1) {
    let highest = Math.max.apply(Math, variants.map((p) => p.price));
    let lowest = Math.min.apply(Math, variants.map((p) => p.price));
    pricing = lowest + '-' + highest;
  }

  let offerLabel = null;
  const offerVariant = variants.filter((v) => v.offer != null);
  if(offerVariant.length) {
    const offer = offerVariant[0].offer;
    offerLabel = (
      <View style={styles.label}>
        <Text style={styles.labelText}>{offer.text}</Text>
      </View>
    )
  } else if(product.offer && product.offer.trim()) {
    offerLabel = (
      <View style={styles.label}>
        <Text style={styles.labelText}>{product.offer}</Text>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        {offerLabel}
        <View style={styles.imgContainer}>
          <TouchableOpacity onPress={onPress}>
            <Image
              source={image}
              style={styles.image}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.footContainer}>
          <View style={styles.textContainer}>
            <Text style={styles.titleText} lineBreakMode="tail" numberOfLines={1}>{product.name}</Text>
          </View>
          <View style={styles.buttonContainer}>
            <Text style={styles.priceText}>{pricing + ' ' + settings.priceText}</Text>
            <TouchableHighlight style={styles.button} onPress={add.bind(this, product)} underlayColor="#51c151">
              <Text style={styles.buttonText}>ADD</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    </View>
  );

}

ProductTile.propTypes = {
  product: React.PropTypes.object.isRequired,
  onPress: React.PropTypes.func,
  add: React.PropTypes.func,
};

ProductTile.defaultProps = {
  onPress: () => console.log('Product clicked'),
  add: () => console.log('Product add'),
}

export default ProductTile;
