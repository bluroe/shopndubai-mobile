import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  container: {
    marginLeft: 15,
    flex: .5,
    shadowColor: '#000',
    shadowRadius: 3,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: .2,
    // borderWidth: 1,
    // borderColor: "#ccc",
    elevation: 2,
    // overflow: 'hidden',
    backgroundColor: '#fff'
  },
  content: {
    padding: 10,
    overflow:'hidden'
  },
  imgContainer: {
    justifyContent: 'center',
    height: 170
  },
  image: {
    width: null,
    height: 150,
    backgroundColor: '#fff'
  },
  footContainer: {
    justifyContent: 'center'
  },
  textContainer: {
    marginBottom: 5
  },
  titleText: {
    color: '#555',
    fontSize: 14,
    fontFamily: 'cabin'
  },
  priceText: {
    color: '#999',
    fontSize: 13,
    flex: 1,
    fontFamily: 'cabin'
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  button: {
    backgroundColor: colors.accentBtnBg,
    padding: 5,
    borderRadius: 20,
    paddingRight: 10,
    paddingLeft: 10,
  },
  buttonText: {
    color: '#fff',
    fontSize: 11,
    fontFamily: 'cabin'
  },
  label: {
    position: 'absolute',
    right: -25,
    top: 25,
    backgroundColor: '#FF6749',
    height: 20,
    width: 120,
    justifyContent: 'center',
    alignItems: 'center',
    transform: [
      {rotate: '45deg'}
    ],
    zIndex: 1
  },
  labelText: {
    color: '#fff',
    fontSize: 11,
    textAlign: 'center'
  }
});
