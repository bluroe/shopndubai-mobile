import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableHighlight,
  StyleSheet,
  Navigator,
  Platform,
  BackAndroid,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import { switchTab } from '../../actions/navigation';
import {
  addAddress,
  editAddress
} from '../../actions/customer';
import Home from '../../routes/Home';
import Category from '../../routes/Category';
import ProductListing from '../../routes/ProductListing';
import ProductView from '../../routes/ProductView';
import Search from '../../routes/Search';
import Cart from '../../routes/Cart';
import DeliveryAddress from '../../routes/DeliveryAddress';
import OrderConfirmation from '../../routes/OrderConfirmation';
import OrderPlaced from '../../routes/OrderPlaced';
import Orders from '../../routes/Orders';
import OrderView from '../../routes/OrderView';
import Profile from '../../routes/Profile';
import styles from './styles';
import settings from '../../config/settings';
import images from '../../config/images';
import TabLayout from '../../layouts/TabLayout';

class Navigation extends Component {

  static childContextTypes = {
    addBackButtonListener: React.PropTypes.func,
    removeBackButtonListener: React.PropTypes.func,
  };

  constructor() {
    super();
    this._handlers = [];
    this.addBackButtonListener = this.addBackButtonListener.bind(this);
    this.removeBackButtonListener = this.removeBackButtonListener.bind(this);
    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    const navigator = this.layout.refs.wrappedInstance.getNavigator();
    if(navigator && navigator.getCurrentRoutes().length > 1) {
      navigator.pop();
      return true;
    }
    if(this.props.tab !== 'home') {
      this.props.dispatch(switchTab('home'));
      return true;
    }
    Alert.alert(
      'Exit',
      'Are you sure you want to exit?',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'Exit', onPress: () => BackAndroid.exitApp(0)},
      ]
    );
    return true;
  }

  getChildContext() {
    return {
      addBackButtonListener: this.addBackButtonListener,
      removeBackButtonListener: this.removeBackButtonListener,
    };
  }

  addBackButtonListener(listener) {
    this._handlers.push(listener);
  }

  removeBackButtonListener(listener) {
    this._handlers = this._handlers.filter((handler) => handler !== listener);
  }

  /* tab methods */

  renderHomeScene(route, navigator) {
    switch (route.id) {
      case 'category':
        return <Category category={route.category} navigator={navigator} />;
      case 'productListing':
        return <ProductListing category={route.category} navigator={navigator} />;
      case 'productView':
        return <ProductView product={route.product} navigator={navigator} />;
      case 'search':
        return <Search navigator={navigator} />;
      default:
        return <Home navigator={navigator} />
    }
  }

  renderCartScene(route, navigator) {
    switch (route.id) {
      case 'deliveryAddress':
        return <DeliveryAddress navigator={navigator} />;
      case 'orderConfirmation':
        return <OrderConfirmation navigator={navigator} />;
      case 'orderPlaced':
        return <OrderPlaced navigator={navigator} msg={route.msg} />;
      default:
        return <Cart navigator={navigator} />;
    }
  }

  renderOrderScene(route, navigator) {
    switch (route.id) {
      case 'orderView':
        return <OrderView navigator={navigator} order={route.order} />;
      default:
        return <Orders navigator={navigator} />;
    }
  }

  render() {
    return (
      <TabLayout
        ref={layout => this.layout = layout}
        renderHomeScene={this.renderHomeScene}
        renderCartScene={this.renderCartScene}
        renderOrderScene={this.renderOrderScene}
      />
    );
  }

}

function select(store) {
  return {
    tab: store.navigation.tab
  }
}

export default connect(select)(Navigation);
