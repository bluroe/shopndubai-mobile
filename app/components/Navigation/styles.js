import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  button: {
    padding: 10,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
