import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import styles from './styles';

const Loading = (props) => {
  const {
    style,
    ...restProps
  } = props;
  return (
    <View style={[styles.container, style]}>
      <ActivityIndicator
        animating
        size={props.size}
        {...restProps}
      />
    </View>
  );
};

Loading.propTypes = {
  size: React.PropTypes.string,
};

Loading.defaultProps = {
  size: 'large',
  style: []
};

export default Loading;
