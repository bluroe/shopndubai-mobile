import { create } from '../../common/AppStyleSheet';

export default create({
  container: {
    flex: 1,
    ios: {
      marginBottom: 50
    }
  },
  textContainer: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 10
  },
  textContent: {
    flexGrow: 1
  },
  text: {
    fontFamily: 'cabin',
    color: '#555'
  },
  emptyMessage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  emptyText: {
    fontSize: 20
  },
  content: {
    flex: 1
  },
  list: {
    flexGrow: 1
  },
  listItem: {
  },
  footer: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#efefef',
    flexBasis: 1
  },
  footerCont: {
    flexDirection:'row'
  },
  footerText: {
    fontFamily: 'cabin',
    fontSize: 20,
    color: '#555'
  }
});
