import React, { Component } from 'react';
import {
  View,
  Text,
  ListView,
  TouchableHighlight,
  RefreshControl,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import styles from './styles';
import AppHeader from '../../common/AppHeader';
import Loading from '../../components/Loading';
import ErrorPage from '../../common/ErrorPage';

const Services = (props) => {
  const {
    loading,
    navigator,
    services,
    ds,
    error,
    loadServices,
    placeCall,
    phone
  } = props;

  const _renderRows = (row) => {
    const available = row.available > 0;
    return (
      <View style={styles.listItem}>
        <View style={styles.textContainer}>
          <View style={styles.textContent}>
            <Text style={[styles.text, {marginBottom: 2}]}>{row.name}</Text>
            <Text style={[styles.text, {color: available ? '#2ECC40' : '#FF4136'}]}>{available ? 'Available' : 'Service Full'}</Text>
          </View>
          <View>
            <Text style={styles.text}>{row.area}</Text>
          </View>
        </View>
      </View>
    );
  }

  const _renderSeparator = (sectionId, rowId, adjacentRowHighlighted) => {
    return (
      <View
        key={`${sectionId}-${rowId}`}
        style={{
          height: 1,
          backgroundColor: '#ccc'
        }}
      />
    )
  }

  const refreshControl = (
    <RefreshControl
      refreshing={loading}
      onRefresh={loadServices}
    />
  )

  let content = null;
  if(loading && services.length == 0) {
    content = <Loading />;
  } else if(error) {
    content = <ErrorPage onPress={loadServices} />;
  } else if(services.length) {
    const contact = phone
      ? <Text style={styles.footerText}>{phone}</Text>
      : <ActivityIndicator size="small" />;
    content = (
      <View style={styles.content}>
        <ListView
          dataSource={ds.cloneWithRows(services)}
          refreshControl={refreshControl}
          renderRow={_renderRows}
          renderSeparator={_renderSeparator}
          style={styles.list}
        />
        <TouchableHighlight onPress={placeCall} style={styles.footer} underlayColor="#ccc">
          <View style={styles.footerCont}>
            <Text style={styles.footerText}>Call : </Text>
            {contact}
          </View>
        </TouchableHighlight>
      </View>
    );
  } else {
    content = (
      <View style={styles.emptyMessage}>
        <Text style={styles.emptyText}>No services</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <AppHeader
        title="Services"
        style={{backgroundColor:'#2ecc71'}}
        navigator={navigator}
      />
      {content}
    </View>
  );
}

export default Services;
