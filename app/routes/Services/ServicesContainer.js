import React from 'react';
import {
  ListView,
  Linking,
  Platform
} from 'react-native';
import Services from './Services';
import axios from 'axios';
import settings from '../../config/settings';

class ServicesContainer extends React.Component {

  constructor() {
    super();
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      services: [],
      loading: true,
      error: false,
      phone: null
    }
    this.loadServices = this.loadServices.bind(this);
    this.loadContactNumber = this.loadContactNumber.bind(this);
  }

  componentWillUnmount() {
    this.cancel();
    this.contactCancel();
  }

  componentDidMount() {
    this.loadServices();
    this.loadContactNumber();
  }

  placeCall() {
    const { phone } = this.state;
    if(phone) {
      Linking.openURL('tel:' + phone);
    } else {
      this.contactCancel();
      this.loadContactNumber();
    }
  }

  loadContactNumber() {
    const query = `
      {
        settings(key: "contact_no") {
          value
        }
      }
    `;
    const url = settings.serverURL + '/graphql?query=' + query;
    axios.get(url, {
      cancelToken: new axios.CancelToken((c) => {
        this.contactCancel = c;
      })
    })
      .then((rsp) => {
        const phone = rsp.data.data.settings[0].value;
        console.log(phone)
        this.setState({phone})
      })
      .catch((err) => {
          this.setState({
            loading: false,
            error: true
          })
          if (error.response) {
            // The request was made, but the server responded with a status code
            // that falls out of the range of 2xx
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          } else {
            // Something happened in setting up the request that triggered an Error
            // console.log('Error', error.message);
          }
          // console.log(error.config);
      });
  }

  loadServices() {
    this.setState({
      loading: true,
      error: false
    })
    const query = `
      {
        services {
          name,
          available,
          total,
          area
        }
      }
    `;
    const url = settings.serverURL + '/graphql?query=' + query;
    axios.get(url, {
      cancelToken: new axios.CancelToken((c) => {
        this.cancel = c;
      })
    })
      .then((rsp) => {
        const { services } = rsp.data.data;
        this.setState({
          services,
          loading: false
        })
      })
      .catch((error) => {
        this.setState({
          loading: false,
          error: true
        })
        if (error.response) {
          // The request was made, but the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else {
          // Something happened in setting up the request that triggered an Error
          // console.log('Error', error.message);
        }
        // console.log(error.config);
      });
  }

  render() {
    return (
      <Services
        ds={this.ds}
        loadServices={this.loadServices}
        placeCall={this.placeCall.bind(this)}
        {...this.state}
      />
    );
  }
}

export default ServicesContainer;
