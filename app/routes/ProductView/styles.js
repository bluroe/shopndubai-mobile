import { Dimensions, Platform } from 'react-native';
import { create } from '../../common/AppStyleSheet';

const { width } = Dimensions.get('window');

export default create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    ios: {
      marginBottom: 50
    }
  },
  slideContainer: {
    backgroundColor: '#fff',
    shadowOpacity: 0.3,
    shadowRadius: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0
    },
    elevation: 2,
  },
  imgSlider: {
    android: {
      flex: 1,
    },
  },
  imgContainer: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    // width,
    height: 250,
    backgroundColor: '#fff'
  },
  image: {
    width: null,
    height: 210
  },
  content: {
    backgroundColor: '#fff',
    flexGrow: 1,
    marginTop: 10,
    marginBottom: 10,
    elevation: 2,
    shadowOpacity: 0.3,
    shadowRadius: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0
    },
  },
  textContainer: {
    paddingHorizontal: 10,
    paddingTop: 15,
    paddingBottom: 15,
  },
  name: {
    color: '#555',
    fontSize: 30,
    textAlign: 'center',
    fontFamily: 'cabin',
    marginBottom: 10
  },
  price: {
    color: '#555',
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'cabin'
  },
  extra_text_cont: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  extra_text: {
    color: '#555',
    fontFamily: 'cabin'
  },
  qtyContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  qtyField: {
    width: 50,
    height: 30,
    marginHorizontal: 5,
    textAlign: 'center',
    borderRadius: 15,
    borderColor: '#ccc',
    borderWidth: 1,
    android: {
      paddingTop: 0,
      paddingBottom: 0
    }
  },
  miniBtn: {
    borderRadius: 15,
    borderColor: '#ccc',
    borderWidth: 1,
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    tintColor: '#555'
  },
  pickerToggle: {
    alignSelf: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#ccc',
    height: 30,
    paddingHorizontal: 10,
    borderRadius: 15,
  },
  pickerToggleText: {
    marginRight: 5
  },
  picker: {
    android: {
      alignSelf: 'center',
      width: 140,
    }
  },
  pickerConfirm: {
    alignItems: 'center',
    backgroundColor: '#ccc',
    borderRadius: 4,
    padding: 10,
    marginHorizontal: 5,
    marginBottom: 5
  },
  pickerConfirmText: {
    color: '#555'
  },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    ios: {
      paddingVertical: 5
    },
    flexDirection: 'row',
    backgroundColor: '#fff',
    elevation: 2,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0
    },
  },
  descContainer: {
    backgroundColor: '#fff',
    marginBottom: 10,
    elevation: 2,
    shadowOpacity: 0.3,
    shadowRadius: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0
    },
  },
  descContent: {
    paddingHorizontal: 15,
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: '#fff'
  },
  descHead: {
    fontSize: 18,
    fontFamily: 'cabin',
    color: '#555',
    marginBottom: 5
  },
  descText: {
    fontFamily: 'cabin',
    color: '#555'
  }
});
