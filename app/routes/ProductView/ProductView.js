import React from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  TextInput,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  Picker,
  PickerIOS,
  Platform,
  Animated
} from 'react-native';
import Image from 'react-native-image-progress';
import Swiper from 'react-native-swiper';
import AppHeader from '../../common/AppHeader';
import Button from '../../components/Button';
import styles from './styles';
import images from '../../config/images';
import settings from '../../config/settings';
import CartModal from '../../common/CartModal';

const ProductView = (props) => {

  const {
    add,
    product,
    loading,
    qty,
    setQty,
    incrementQty,
    decrementQty,
    navigator,
    variant,
    setVariant,
    openCart,
    cartItems,
    setModal,
    showPicker,
    togglePicker,
    fadeAnim,
    setPickerHeight
  } = props;

  const cartModal = <CartModal ref={(ref) => setModal(ref)} />

  const _renderSlides = () => {
    if(product.images.length) {
      return product.images.map((image, key) => {
        var imagePath = (image.filename.startsWith('/') ? settings.serverURL : '') + image.filename;
        return (
          <View style={styles.imgContainer} key={key}>
            <Image source={{uri: imagePath}} style={styles.image} resizeMode="contain"/>
          </View>
        );
      })
    }
    return (
      <View style={styles.imgContainer}>
        <Image source={images.noimage} style={styles.image} resizeMode="contain"/>
      </View>
    );
  }

  let description = null;
  if(product.description) {
    description = (
      <View style={styles.descContainer}>
        <View style={styles.descContent}>
          <Text style={styles.descHead}>Description</Text>
          <Text style={styles.descText}>{product.description}</Text>
        </View>
      </View>
    );
  }

  const { variants } = product;
  const selectedVariant = variants.filter(v => v.id == variant)[0];
  const priceSource = selectedVariant.offer && selectedVariant.offer.price ? selectedVariant.offer : selectedVariant;
  let priceText = priceSource.price + ' ' + settings.priceText;
  if(variants.length == 1) {
    priceText += ((product.packet === true ? ' for ' : ' / ') + selectedVariant.type);
  }

  const pickerText = (
      <TouchableHighlight onPress={togglePicker} style={styles.pickerToggle} underlayColor="#ccc">
        <View style={{flexDirection:'row'}}>
          <Text style={styles.pickerToggleText}>{selectedVariant.type}</Text>
          <Image source={images.caret} />
        </View>
      </TouchableHighlight>
  )

  const picker = (
    <View style={{}}>
      <Picker
        selectedValue={variant}
        onValueChange={(v) => setVariant(v)}
        mode="dropdown"
        style={styles.picker}>
        {variants.map((p, index) => {
          return <Picker.Item key={index} label={p.type} value={p.id} />
        })}
      </Picker>
    </View>
  )

  let extra_text = null;
  if(selectedVariant.offer) {
    extra_text = (
      <View style={styles.extra_text_cont}>
        <Text>
          {selectedVariant.offer.price && (
          <Text>
            <Text style={[styles.extra_text, {textDecorationLine:'line-through', color: '#FF4136'}]}>{selectedVariant.price + ' RS' }</Text>
            <Text> - </Text>
          </Text>
          )}
          <Text style={[styles.extra_text, selectedVariant.offer.price ? null : {color:'#2ECC40'}]}>{selectedVariant.offer.text}</Text>
        </Text>
      </View>
    );
  }

  console.log(cartItems)
  const cartItem = {
    title: 'cart',
    icon: cartItems ? images.cart_white_f : images.cart_white,
    onPress: openCart,
  }

  return (
    <View style={styles.container}>
      <AppHeader
        title={product.name}
        style={{backgroundColor:'#3498db'}}
        extraItems={[cartItem]}
        navigator={navigator}
      />
      <ScrollView style={{flex: 1}} contentContainerStyle={{backgroundColor: '#efefef'}}>
        <View style={styles.slideContainer}>
          <Swiper style={styles.imgSlider} height={250}>
            {_renderSlides()}
          </Swiper>
        </View>
        <View style={styles.content}>
          <View style={styles.textContainer}>
            <Text style={styles.name}>{product.name}</Text>
            <Text style={styles.price}>{priceText}</Text>
            {extra_text}
          </View>
          {variants.length > 1 && (Platform.OS === 'ios' ? pickerText : picker)}
          <View style={styles.qtyContainer}>
            <TouchableHighlight onPress={decrementQty} style={styles.miniBtn} underlayColor="#ccc">
              <Image source={images.remove} style={styles.icon} />
            </TouchableHighlight>
            <TextInput
              style={styles.qtyField}
              value={qty + ''}
              onChangeText={(qty) => setQty(qty)}
              keyboardType="numeric"
              underlineColorAndroid="transparent"
            />
            <TouchableHighlight onPress={incrementQty} style={styles.miniBtn} underlayColor="#ccc">
              <Image source={images.add} style={styles.icon} />
            </TouchableHighlight>
          </View>
        </View>
        {description}
      </ScrollView>
      <View style={styles.buttonContainer}>
        <Button onPress={add.bind(this, product)} text="ADD TO CART" />
      </View>
      {Platform.OS === 'ios' && (
        <Animated.View style={{height: fadeAnim}}>
          <View onLayout={setPickerHeight}>
            {picker}
            <TouchableHighlight onPress={togglePicker} style={styles.pickerConfirm} underlayColor="#999">
              <Text style={styles.pickerConfirmText}>OK</Text>
            </TouchableHighlight>
          </View>
        </Animated.View>
      )}
      {cartModal}
    </View>
  );

}

export default ProductView;
