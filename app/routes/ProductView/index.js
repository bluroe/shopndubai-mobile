import ProductView from './ProductView';
import ProductViewContainer from './ProductViewContainer';

export { ProductView };
export default ProductViewContainer;
