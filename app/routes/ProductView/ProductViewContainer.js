
import React, { Component, PropTypes } from 'react';
import {
  ToastAndroid,
  Platform,
  Animated,
  Easing
} from 'react-native';
import { connect } from 'react-redux';
import ProductView from './ProductView';
import Cart from '../../routes/Cart';
import settings from '../../config/settings';
import { addToCart, updateDeliveryCharge } from '../../actions/cart';
import { switchTab } from '../../actions/navigation';


class ProductViewContainer extends Component {

  static propTypes = {
    product: PropTypes.object.isRequired
  }

  static contextTypes = {
    openDrawer: PropTypes.func
  }

  constructor() {
    super();
    this.state = {
      qty: 1,
      variant: null,
      // ios
      showPicker: false,
      fadeAnim: new Animated.Value(0),
      pickerHeight: 0
    }
    this.incrementQty = this.incrementQty.bind(this);
    this.decrementQty = this.decrementQty.bind(this);
  }

  togglePicker() {
    const showPicker = !this.state.showPicker;
    this.setState({
      showPicker: showPicker
    });
    Animated.timing(this.state.fadeAnim, {
      toValue: showPicker ? this.state.pickerHeight : 0,
      // easing: Easing.back,
      duration: 200
    }).start();
  }

  componentDidMount() {
    const { min_qty, max_qty } = this.props.product;
    if(min_qty) { this.min_qty = min_qty; }
    if(max_qty) { this.max_qty = max_qty; }
  }

  componentWillMount() {
    const variant = this.props.product.variants[0].id;
    this.setState({
      variant
    });
  }

  add(product) {
    const { variant, qty, showPicker } = this.state
    this.props.dispatch(addToCart(product, variant, qty));
    if(this.state.showPicker) this.togglePicker();
    this.modal.showModal();
    this.props.dispatch(updateDeliveryCharge());
  }

  setModal(modal) {
    this.modal = modal;
  }

  incrementQty() {
    const { qty } = this.state;
    if(this.max_qty && this.max_qty === qty) return;
    this.setState({ qty: parseInt(qty) + 1 })
  }

  decrementQty() {
    const { qty } = this.state;
    if(this.min_qty && this.min_qty === qty) return;
    if(qty === 1) return;
    this.setState({ qty: parseInt(qty) - 1 })
  }

  openCart() {
      this.props.dispatch(switchTab('cart'));
  }

  setPickerHeight(event) {
    console.log(event.nativeEvent.layout.height)
    this.setState({
      pickerHeight: event.nativeEvent.layout.height
    });
  }

  render() {
    return (
      <ProductView
        add={this.add.bind(this)}
        product={this.props.product}
        incrementQty={this.incrementQty}
        decrementQty={this.decrementQty}
        setQty={(qty) => this.setState({qty})}
        setVariant={(variant) => this.setState({variant})}
        openCart={this.openCart.bind(this)}
        cartItems={this.props.cartItems}
        setModal={this.setModal.bind(this)}
        togglePicker={this.togglePicker.bind(this)}
        setPickerHeight={this.setPickerHeight.bind(this)}
        navigator={this.props.navigator}
        {...this.state}
      />
    );
  }

}

const mapStates = (store) => {
  return {
    cartItems: store.cart.items.length
  }
}

export default connect(mapStates)(ProductViewContainer);
