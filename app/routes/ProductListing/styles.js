import { create } from '../../common/AppStyleSheet';

export default create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
    ios: {
      marginBottom: 50
    }
  },
  messageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  messageText: {
    fontSize: 25,
    fontFamily: 'cabin',
    color: '#555'
  },
  row: {
    paddingTop: 8,
    paddingRight: 15,
    paddingBottom: 7,
    flexDirection: 'row'
  },
  miniLoader: {
    marginBottom: 10
  }
});
