import React, { Component, PropTypes } from 'react';
import { ToastAndroid, Platform, Animated } from 'react-native';
import { connect } from 'react-redux';
import axios from 'axios';
import { addToCart, updateDeliveryCharge } from '../../actions/cart';
import { switchTab } from '../../actions/navigation';
import ProductView from '../../routes/ProductView';
import ProductListing from './ProductListing';
import settings from '../../config/settings';

class ProductListingContainer extends Component {

  static propTypes = {
    category: PropTypes.object.isRequired,
  }

  static contextTypes = {
    openDrawer: PropTypes.func
  }

  constructor() {
    super();
    this.state = {
      products: [],
      loading: true,
      ad: null,
      offset: 0,
      total: 0,
      finished: false,
      error: false
    }
    this.open = this.open.bind(this);
  }

  componentDidMount() {
    this.loadMore();
    // fetch(settings.apiURL + 'advertisement')
    //   .then((response) => response.json())
    //   .then((data) => this.setState({ad: data.image}))
  }

  componentWillUnmount() {
    this.cancel();
  }

  setModal(modal) {
    this.modal = modal;
  }

  open(product) {
    this.props.navigator.push({
      id: 'productView', product
    });
  }

  add(product) {
    let variant = product.variants.filter(p => p.offer)[0];
    this.props.dispatch(addToCart(product, variant ? variant.id : null));
    this.modal.showModal();
    this.props.dispatch(updateDeliveryCharge());
  }

  openCart() {
    this.props.dispatch(switchTab('cart'));
  }

  openSearch() {
    this.props.navigator.push({id:'search'});
  }

  loadMore() {
    this.setState({loading:true});
    if(this.state.total != 0 && this.state.offset == this.state.total) {
      // console.log('all products loaded')
      this.setState({
        finished: true,
        loading: false
      });
      return;
    }
    const { id } = this.props.category;
    const query = `
      {
        products(category_id:${id}, offset: ${this.state.offset}) {
          items {
            name,
            offer,
            packet,
            description,
            images {
              filename
            },
            variants {
              type,
              price,
              id,
              offer {
                qty,
                price,
                add_qty,
                text
              }
            }
          },
          total
        }
      }
    `;
    const url = settings.serverURL + '/graphql?query=' + query;
    // console.log(url)
    axios.get(url, {
      cancelToken: new axios.CancelToken((c) => {
        this.cancel = c;
      })
    })
      .then((rsp) => {
        // console.log(rsp.data)
        const responseJson = rsp.data;
        // console.log(responseJson);
        var products = responseJson.data.products.items;
        var total = responseJson.data.products.total;
        // console.log(products.length + ' products loaded');
        this.setState({
          loading: false,
          products: this.state.products.concat(products),
          offset: this.state.offset + products.length,
          total
        });
      })
      .catch((error) => {
        this.setState({
          error: true,
          loading: false
        })
          if (error.response) {
            // The request was made, but the server responded with a status code
            // that falls out of the range of 2xx
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          } else {
            // Something happened in setting up the request that triggered an Error
            // console.log('Error', error.message);
          }
          // console.log(error.config);
      });
  }

  render() {
    return (
      <ProductListing
        add={this.add.bind(this)}
        open={this.open}
        category={this.props.category}
        openSearch={this.openSearch.bind(this)}
        openCart={this.openCart.bind(this)}
        loadMore={this.loadMore.bind(this)}
        navigator={this.props.navigator}
        setModal={this.setModal.bind(this)}
        cartItems={this.props.cartItems}
        {...this.state}
      />
    );
  }

}

const mapStates = store => ({
  customerKey: store.customer.key,
  cartItems: store.cart.items.length
})

export default connect(mapStates)(ProductListingContainer);
