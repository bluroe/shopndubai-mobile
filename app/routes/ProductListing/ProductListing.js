import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  Platform,
  ActivityIndicator,
  Animated,
  TouchableWithoutFeedback
} from 'react-native';
import AppHeader from '../../common/AppHeader';
import ProductTile from '../../components/ProductTile';
import styles from './styles';
import Loading from '../../components/Loading';
import images from '../../config/images';
import settings from '../../config/settings';
import CartModal from '../../common/CartModal';
import ErrorPage from '../../common/ErrorPage';

const ProductListing = (props) => {

  const {
    open,
    add,
    products,
    loading,
    category,
    navigator,
    openSearch,
    openCart,
    ad,
    loadMore,
    finished,
    setModal,
    error,
    cartItems
  } = props;

  const renderRows = (products) => {
    const rows = [];
    for(var i=0; i < products.length; i+=2) {
      var items = products.slice(i, i+2).map(function(product, index) {
        return <ProductTile onPress={open.bind(this, product)} add={add} product={product} key={index} />;
      });
      if(items.length != 2) {
        items.push(<View style={{flex:.5,marginLeft:15}} key="2" />);
      }
      rows.push(
        <View style={styles.row} key={i}>
          {items}
        </View>
      );
    }
    return rows;
  }

  const modal = <CartModal ref={(ref) => setModal(ref)} />;

  const searchItem = {
    title: 'search',
    icon: images.search_white,
    onPress: openSearch,
  }

  const cartItem = {
    title: 'cart',
    icon: cartItems ? images.cart_white_f : images.cart_white,
    onPress: openCart,
  }

  let advertisement = null;
  if(ad) {
    const image = settings.serverURL + ad;
    advertisement = (
      <View style={{height:100,backgroundColor:'#fff'}}>
        <Image source={{uri: image}} style={{height:100,width:null}} resizeMode="cover" />
      </View>
    );
  }

  const isCloseEnoughToBottom = (nativeEvent) => {
    const padding = 20;
    return nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >= nativeEvent.contentSize.height - padding;
  }

  let content = null;
  if(loading && products.length == 0) {
    content = <Loading />;
  } else if(error && products.length == 0) {
    content = <ErrorPage onPress={loadMore} />;
  } else if(products.length) {
    content = (
      <ScrollView
        onScroll={({nativeEvent}) => {
          if(isCloseEnoughToBottom(nativeEvent) && !loading && !finished) {
            console.log('loading more', products.length)
            loadMore();
          }
        }}
        scrollEventThrottle={400}
      >
        {advertisement}
        <View style={{paddingTop:10,paddingBottom:10}}>
          {renderRows(products)}
        </View>
        {loading && <ActivityIndicator animating style={styles.miniLoader} />}
      </ScrollView>
    )
  } else {
    content = (
      <View style={styles.messageContainer}>
        <Image source={images.whitebox} />
        <Text style={styles.messageText}>No products</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <AppHeader
        title={category.name}
        style={{backgroundColor:'#2ecc71'}}
        rightItem={searchItem}
        extraItems={[cartItem]}
        navigator={navigator}
      />
      {content}
      {modal}
    </View>
  );

}

export default ProductListing;
