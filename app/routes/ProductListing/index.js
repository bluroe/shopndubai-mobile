import ProductListingContainer from './ProductListingContainer';
import ProductListing from './ProductListing';

export { ProductListing };
export default ProductListingContainer;
