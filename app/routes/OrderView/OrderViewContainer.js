import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListView } from 'react-native';
import OrderView from './OrderView';

class OrderViewContainer extends Component {

  componentDidMount() {
  }

  render() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    console.log(this.props.order);
    const { orderItems } = this.props.order;
    return (
      <OrderView
        id={this.props.order.id}
        items={ds.cloneWithRows(orderItems)}
        deliveryCharge={this.props.order.delivery_charge}
        total={this.props.order.total}
        navigator={this.props.navigator}
      />
    )
  }

}

export default connect()(OrderViewContainer);
