import { create } from '../../common/AppStyleSheet';

export default create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    ios: {
      marginBottom: 50
    }
  },
  content: {
    flexGrow: 1,
    flexBasis: 1
  },
  heading: {
    fontSize: 20,
    color: '#555',
    fontFamily: 'cabin'
  },
  listHead: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    borderColor: '#ccc',
    borderBottomWidth: 1,
    padding: 10
  },
  list: {
    flexGrow: 1,
  },
  row: {
    flexDirection: 'row',
    padding: 10
  },
  icon: {
    width: 50,
    height: 50,
    marginRight: 5
  },
  nameText: {
    flexGrow: 1,
    flexBasis: 1,
    color: '#555',
    fontFamily: 'cabin'
  },
  priceText: {
    color: '#555',
    fontFamily: 'cabin'
  },
  totalContainer: {
    flexDirection: 'row',
    padding: 10,
    flexDirection: 'row',
    borderColor: '#ccc',
    borderTopWidth: 1,
    backgroundColor: '#fff'
  },
  totalContent: {
    marginRight: 10,
  },
  totalText: {
    color: '#555',
    fontSize: 16,
    fontFamily: 'cabin'
  }
});
