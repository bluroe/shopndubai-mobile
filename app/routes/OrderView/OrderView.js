import React from 'react';
import {
  View,
  Text,
  ScrollView,
  ListView,
  Image
} from 'react-native';
import styles from './styles';
import AppHeader from '../../common/AppHeader';
import settings from '../../config/settings';
import images from '../../config/images';

const OrderView = (props) => {

  const { id, items, total, deliveryCharge, navigator } = props;

  const _renderRow = (rowData) => {
    return (
      <View style={styles.row}>
        <Image source={rowData.image ? {uri: settings.serverURL + rowData.image} : images.noimage} style={styles.icon} resizeMode="contain" />
        <Text style={styles.nameText}>{rowData.name + ' x ' + rowData.qty}</Text>
        <Text style={styles.priceText}>{(rowData.price * rowData.qty) + ' ' + settings.priceText}</Text>
      </View>
    );
  }

  const _renderSeparator = (sectionId, rowId, adjacentRowHighlighted) => {
    return (
      <View
        key={`${sectionId}-${rowId}`}
        style={{
          height: 1,
          backgroundColor: '#ccc'
        }}
      />
    );
  };


  return (
    <View style={styles.container}>
      <AppHeader
        title={'Order Details'}
        style={{backgroundColor:'#d35400'}}
        navigator={navigator}
      />
      <View style={styles.content}>
        <View style={styles.listHead}>
          <Text style={styles.heading}>{ 'Order ID #' + id }</Text>
        </View>
        <ScrollView style={{flex:1}}>
          <ListView
            dataSource={items}
            automaticallyAdjustContentInsets={false}
            renderRow={_renderRow}
            renderSeparator={_renderSeparator}
            style={styles.list}
          />
        </ScrollView>
      </View>
      <View style={styles.totalContainer}>
        <View style={styles.totalContent}>
          <Text style={styles.totalText}>Total:</Text>
          <Text style={styles.totalText}>Delivery Charge:</Text>
          <Text style={styles.totalText}>Total:</Text>
        </View>
        <View>
          <Text style={styles.totalText}>{total + ' ' + settings.priceText}</Text>
          <Text style={styles.totalText}>{deliveryCharge + ' ' + settings.priceText}</Text>
          <Text style={styles.totalText}>{(total + deliveryCharge) + ' ' + settings.priceText}</Text>
        </View>
      </View>
    </View>
  )

}

export default OrderView;
