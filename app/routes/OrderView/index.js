import OrderView from './OrderView';
import OrderViewContainer from './OrderViewContainer';

export { OrderView };
export default OrderViewContainer;
