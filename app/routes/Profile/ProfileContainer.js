import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Profile from './Profile';

class ProfileContainer extends Component {

  render() {
    return (
      <Profile />
    )
  }

}

export default connect()(ProfileContainer);
