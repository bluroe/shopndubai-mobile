import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import images from '../../config/images';

const Profile = (props) => {

  return (
    <View style={styles.container}>
      <View style={styles.imgContainer}>
        <Image source={images.probg} style={{flex:1,height: 260}} />
      </View>
      <View style={styles.middle}>
        <View style={styles.midBox}>
          <Image source={images.telyicon} resizeMode="contain" style={{width:50,height:50}} />
        </View>
      </View>
      <View style={styles.content}>
        <View style={{alignItems:'center',paddingBottom: 10}}>
          <Text style={{color:'#f00'}}>Not logged in</Text>
        </View>
        <View style={styles.list}>
          <TouchableHighlight onPress={() => Actions.orders()} style={styles.link}>
            <View style={styles.row}>
              <Image source={images.home} style={styles.rowIcon} />
              <Text style={[styles.rowText, {flexGrow:1}]}>Orders</Text>
              <Text style={styles.rowText}>12</Text>
            </View>
          </TouchableHighlight>
          <View style={styles.separator} />
          <TouchableHighlight onPress={() => console.log('fooz')} style={styles.link}>
            <View style={styles.row}>
              <Image source={images.home} style={styles.rowIcon} />
              <Text style={[styles.rowText, {flexGrow:1}]}>Saved Cards</Text>
              <Text style={styles.rowText}>2</Text>
            </View>
          </TouchableHighlight>
          <View style={styles.separator} />
          <TouchableHighlight onPress={() => console.log('fooz')} style={styles.link}>
            <View style={styles.row}>
              <Image source={images.home} style={styles.rowIcon} />
              <Text style={[styles.rowText, {flexGrow:1}]}>Wallet</Text>
              <Text style={styles.rowText}>230</Text>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    </View>
  )

}

export default Profile;
