import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 65,
    marginBottom: 50,
    backgroundColor: '#efefef'
  },
  imgContainer: {
    height: 200,
    flexDirection: 'row',
    backgroundColor: '#ccc',
    overflow: 'hidden'
  },
  middle: {
    alignItems: 'center'
  },
  midBox: {
    marginTop: -50,
    backgroundColor: '#fff',
    height: 100,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    // borderRadius: 50,
    // borderColor: '#ccc',
    // borderWidth: 1
  },
  content: {
    padding: 15
  },
  list: {
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#ccc',
    overflow: 'hidden',
    backgroundColor: '#fff'
  },
  link: {
    padding: 10,
  },
  row: {
    flexDirection: 'row'
  },
  rowText: {
    color: '#555'
  },
  separator: {
    height: 1,
    backgroundColor: '#ccc'
  },
  rowIcon: {
    tintColor: '#ccc'
  }
});
