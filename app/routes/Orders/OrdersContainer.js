import React, { Component } from 'react';
import { ListView } from 'react-native';
import { connect } from 'react-redux';
import Orders from './Orders';
import settings from '../../config/settings';
import { loadOrders } from '../../actions/orders';

class OrdersContainer extends Component {

  static contextTypes = {
    openDrawer: React.PropTypes.func
  }

  static defaultProps = {
    orders: []
  }

  constructor() {
    super();
    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    this.props.dispatch(loadOrders()).then(() => {
      this.setState({
        loading: false
      });
    });
  }

  onRefresh() {
    this.setState({loading: true});
    this.props.dispatch(loadOrders()).then(() => {
      this.setState({
        loading: false
      });
    });
  }

  openOrder(order) {
    this.props.navigator.push({id:'orderView', order});
  }

  render() {
    return (
      <Orders
        dataSource={this.dataSource}
        openOrder={this.openOrder.bind(this)}
        onRefresh={this.onRefresh.bind(this)}
        orders={this.props.orders}
        loading={this.state.loading}
      />
    )
  }

}

const mapStates = (store) => {
  return {
    customer: store.customer,
    orders: store.orders.orders
  }
}

export default connect(mapStates)(OrdersContainer);
