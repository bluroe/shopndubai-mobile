import React from 'react';
import {
  View,
  ListView,
  TouchableOpacity,
  Text,
  ScrollView,
  RefreshControl,
  Image
} from 'react-native';
import AppHeader from '../../common/AppHeader';
import Loading from '../../components/Loading';
import styles from './styles';
import images from '../../config/images';
import settings from '../../config/settings';

const Orders = (props) => {

  const {
    orders,
    openOrder,
    loading,
    dataSource,
    onRefresh
  } = props;

  const renderStatus = (order) => {
    switch(order.status) {
      case 1:
        return 'In transit';
      case 2:
        return 'Delivered';
      default:
        return 'Processing';
    }
  }

  const renderStatusColor = (order) => {
    switch(order.status) {
      case 1:
        return '#FF4136'
      case 2:
        return '#2ECC40';
      default:
        return '#0074D9';
    }
  }

  const renderStatusText = (order) => {
    const color = renderStatusColor(order);
    const statusText = renderStatus(order);
    return <Text style={[styles.cardText, {color}]}>{statusText}</Text>
  }

  const renderRow = (order) => {
    let itemsText = order.orderItems.map((item) => item.name).join(',');
    if(itemsText.length == 0) {
      itemsText = 'No items';
    }
    return (
      <View style={styles.cardContainer}>
        <TouchableOpacity onPress={() => openOrder(order)}>
          <View style={styles.card}>
            <View style={{flexGrow:1,flexBasis:1}}>
              <Text style={styles.cardHead}>{'Order #' + order.id}</Text>
              <Text style={styles.cardText} numberOfLines={1} lineBreakMode="tail">{itemsText}</Text>
            </View>
            <View style={{marginLeft:5}}>
              {renderStatusText(order)}
              <Text style={styles.cardText}>{order.total + settings.priceText}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  let content = null;
  if(loading && orders.length == 0) {
    content = <Loading />;
  } else if(orders.length) {
    content = (
      <View style={styles.container}>
        <ListView
          style={styles.list}
          dataSource={dataSource.cloneWithRows(orders)}
          renderRow={renderRow}
          automaticallyAdjustContentInsets={false}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={onRefresh}
            />
          }
        />
      </View>
    )
  } else {
    content = (
      <View style={styles.emptyContainer}>
        <Image source={images.empty} style={styles.messageIcon} />
        <Text style={styles.messageText}>No orders</Text>
      </View>
    )
  }

  return (
    <View style={{flex: 1}}>
      <AppHeader
        title={"Orders" + (orders.length ? ' (' + orders.length + ')' : '')}
        style={{backgroundColor:'#e74c3c'}}
      />
      {content}
    </View>
  );
}

export default Orders;
