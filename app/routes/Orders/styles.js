import { create } from '../../common/AppStyleSheet';

export default create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    ios: {
      marginBottom: 50
    }
  },
  list: {
    backgroundColor: '#fafafa',
    paddingVertical: 5,
  },
  cardContainer: {
  },
  card: {
    marginHorizontal: 10,
    marginVertical: 5,
    paddingTop: 15,
    paddingHorizontal: 15,
    backgroundColor: '#fff',
    // borderRadius: 4,
    shadowColor: '#000',
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: .3,
    elevation: 2,
    flexDirection: 'row'
  },
  cardHead: {
    fontFamily: 'cabin',
    fontSize: 16,
    color: '#555',
    marginBottom: 10
  },
  cardText: {
    fontFamily: 'cabin',
    color: '#999',
    marginBottom: 10
  },
  emptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  messageIcon: {
    tintColor: '#ccc',
    marginBottom: 20
  },
  messageText: {
    fontSize: 24,
    color: '#555',
    fontFamily: 'cabin'
  }
});
