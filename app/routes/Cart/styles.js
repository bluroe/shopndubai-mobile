import { create } from '../../common/AppStyleSheet';
import { button, buttonText } from '../../common/styles';

export default create({
  container: {
    flex: 1,
    backgroundColor: '#efefef',
    ios: {
      marginBottom: 50
    }
  },
  listContainer: {
    // padding: 10,
    backgroundColor: '#fff',
    flexGrow: 1,
    marginBottom: 10,
    shadowColor: '#000',
    shadowRadius: 1,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: .3,
    elevation: 2
  },
  list: {
  },
  row: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  productTextContainer: {
    flexGrow: 1,
    flexBasis: 1,
    marginRight: 5,
  },
  nameTextContainer: {
    flexDirection: 'row',
  },
  name: {
    color: '#555',
    fontFamily: 'cabin',
    flex: -1
  },
  qtyText: {
    color: '#999',
    minWidth: 35,
  },
  extraText: {
    color: '#999',
  },
  price: {
    color: '#555',
    fontFamily: 'cabin',
  },
  totalContainer: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#fff',
  },
  totalContent: {
    marginRight: 10,
  },
  totalText: {
    color: '#555',
    paddingBottom: 10,
    fontSize: 16,
    fontFamily: 'cabin',
  },
  closeBtn: {
    height: 16,
    width: 16,
    paddingTop: 2,
    paddingLeft: 3,
    borderRadius: 8,
    backgroundColor: '#ccc',
    marginLeft: 10
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    ios: {
      paddingVertical: 5
    },
    flexDirection: 'row',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowRadius: 3,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: .3,
    elevation: 2,
  },
  messageText: {
    fontSize: 25,
    fontFamily: 'cabin',
    color: '#555',
    marginBottom: 10
  },
  subText: {
    color: '#999',
    fontFamily: 'cabin',
    marginBottom: 20
  },
  button,
  buttonText,
});
