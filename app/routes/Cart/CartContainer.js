import React, { Component } from 'react';
import { ListView, Alert } from 'react-native';
import Cart from './Cart';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  removeItem,
  updateDeliveryCharge,
  clearCart,
  removeItems
} from '../../actions/cart';
import axios from 'axios';
import settings from '../../config/settings';
import { switchTab } from '../../actions/navigation';

class CartContainer extends Component {

  static contextTypes = {
    openDrawer: React.PropTypes.func
  }

  constructor() {
    super();
    this.state = {
      freeDeliveryAbove: 0,
      listScrollEnabled: true,
      deliveryChargeCalculating: false
    }
    this.checkout = this.checkout.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.updateDeliveryCharge = this.updateDeliveryCharge.bind(this);
    this.checkItems = this.checkItems.bind(this);
  }

  componentDidMount() {
    this.updateDeliveryCharge();
    this.checkItems();
  }

  updateDeliveryCharge() {
    this.setState({deliveryChargeCalculating:true});
    this.props.dispatch(updateDeliveryCharge())
      .then(() => {
        this.setState({deliveryChargeCalculating:false});
      })
  }

  checkItems() {
    const url = settings.apiURL + 'check-products';
    const items = this.props.items.map((i) => i.id);
    axios.get(url, { items })
      .then((rsp) => {
        rsp = rsp.data;
        if(rsp.unavailable.length) {
          this.props.dispatch(removeItems(rsp.unavailable));
          Alert.alert(
            'Some items not available',
            'Some products are not available at the moment, they have been removed from your cart',
            [
              {text: 'OK'},
            ],
            { cancelable: false }
          );
        }
      })

  }

  componentWillMount() {
    // this.props.dispatch(clearCart());
  }

  checkout() {
    if(this.props.items.length) {
      this.props.navigator.push({id: 'deliveryAddress'});
    } else {
      alert('There a no items in cart');
    }
  }

  removeItem(product) {
    this.props.dispatch(removeItem(product))
    setTimeout(this.updateDeliveryCharge, 500);
  }

  handleShowMenu() {
    this.context.openDrawer();
  }

  handleSwipeInactive(swipeInactive) {
    this.setState({
      listScrollEnabled: swipeInactive
    });
  }

  render() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return (
      <Cart
        checkout={this.checkout}
        removeItem={this.removeItem}
        {...this.props}
        {...this.state}
        handleShowMenu={this.handleShowMenu.bind(this)}
        handleSwipeInactive={this.handleSwipeInactive.bind(this)}
        addItems={() => this.props.dispatch(switchTab('home'))}
        dataSource={ds}
        updateDeliveryCharge={this.updateDeliveryCharge}
      />
    );
  }

}

const mapStates = store => ({
  items: store.cart.items,
  deliveryCharge: store.cart.deliveryCharge
});

export default connect(mapStates)(CartContainer);
