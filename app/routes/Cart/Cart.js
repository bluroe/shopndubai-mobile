import React from 'react';
import {
  ListView,
  Text,
  View,
  TouchableHighlight,
  Image,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import AppHeader from '../../common/AppHeader';
import Button from '../../components/Button';
import styles from './styles';
import settings from '../../config/settings';
import images from '../../config/images';
import Swipeout from 'react-native-swipeout';

const Cart = (props) => {

  const {
    checkout,
    dataSource,
    items,
    removeItem,
    deliveryCharge,
    handleSwipeInactive,
    addItems,
    deliveryChargeCalculating
  } = props;

  const total = items.reduce((price, item) => {
    return price + (item.price * item.qty);
  }, 0);

  const renderRows = (rowData) => {
    const image = rowData.image ? {uri: settings.serverURL + rowData.image} : images.noimage;
    return (
      <Swipeout
        key={rowData.id}
        right={[{
          text: 'Remove',
          type: 'delete',
          onPress: () => removeItem(rowData.id)
        }]}
        scroll={handleSwipeInactive}>
        <TouchableHighlight resizeMode="contain">
          <View style={styles.row}>
            <Image source={image} style={{width:40,height:40,marginRight:10}} resizeMode="contain" />
            <View style={styles.productTextContainer}>
              <View style={styles.nameTextContainer}>
                <Text style={styles.name} lineBreakMode="tail" numberOfLines={1}>{rowData.name}</Text>
                <Text style={styles.qtyText}>{' x ' + rowData.qty}</Text>
              </View>
              <Text style={styles.extraText}>{rowData.type}</Text>
            </View>
            <Text style={styles.price}>{(rowData.price * rowData.qty) + ' ' + settings.priceText}</Text>
          </View>
        </TouchableHighlight>
      </Swipeout>
    );
  }

  const deliveryChargeText = deliveryChargeCalculating
    ? <ActivityIndicator style={{alignSelf:'flex-start',paddingBottom: 10}} animating />
    : <Text style={styles.totalText}>{deliveryCharge + ' ' + settings.priceText}</Text>;

  let cart = null;
  if(items.length) {
    cart = (
      <View style={{flex:1}}>
        <ScrollView style={{flex:1}}>
          <View style={styles.listContainer}>
            <ListView
              dataSource={dataSource.cloneWithRows(items)}
              automaticallyAdjustContentInsets={false}
              renderRow={renderRows}
              renderSeparator={(sectionId, rowId, adjacentRowHighlighted) => {
                return (
                  <View
                    key={`${sectionId}-${rowId}`}
                    style={{
                      height: 1,
                      backgroundColor: '#efefef'
                    }}
                  />
                )
              }}
              style={styles.list}
            />
          </View>
          <View style={styles.totalContainer}>
            <View style={styles.totalContent}>
              <Text style={styles.totalText}>Total:</Text>
              <Text style={styles.totalText}>Delivery Charge:</Text>
              <Text style={styles.totalText}>Total:</Text>
            </View>
            <View>
              <Text style={styles.totalText}>{total + ' ' + settings.priceText}</Text>
              {deliveryChargeText}
              <Text style={styles.totalText}>{(total + deliveryCharge) + ' ' + settings.priceText}</Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.buttonContainer}>
          <Button onPress={checkout} text="CHECKOUT" />
        </View>
      </View>
    )
  } else {
    const text = 'Looks like you haven\'t added anything to your cart.';
    cart = (
      <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#fff'}}>
        <Image source={images.bag} style={{marginBottom:10}} />
        <Text style={styles.messageText}>Cart empty</Text>
        <Text style={styles.subText}>{text}</Text>
        <TouchableHighlight style={styles.button} onPress={addItems} underlayColor="#e25a3f">
          <Text style={styles.buttonText}>ADD SOME</Text>
        </TouchableHighlight>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <AppHeader
        title={"Cart (" + items.length + " items)"}
        style={{backgroundColor:'#e67e22'}}
      />
      {cart}
    </View>
  );

}

export default Cart;
