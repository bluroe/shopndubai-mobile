import React, { PropTypes } from 'react';
import {
  View,
  Image,
  ScrollView,
  TouchableHighlight,
  Text,
  Animated,
  ListView
} from 'react-native';
import images from '../../config/images';
import AppHeader from '../../common/AppHeader';
import styles from './styles';
import Loading from '../../components/Loading';
import settings from '../../config/settings';

const Category = (props) => {

  const { navigator, expand, category, scrollY } = props;

  const ds = new ListView.DataSource({
    rowHasChanged: (r1, r2) => r1 !== r2
  });

  const _renderRow = (subCategory, index) => {
    return (
      <TouchableHighlight onPress={expand.bind(this, subCategory)} style={styles.link} key={index} underlayColor="#ccc">
        <View style={styles.linkContent}>
          <Text style={styles.linkText}>{subCategory.name}</Text>
          <Image source={images.chevronRight} style={styles.icon} />
        </View>
      </TouchableHighlight>
    );
  };

  const _renderSeparator = (sectionId, rowId, adjacentRowHighlighted) => {
    return (
      <View
        key={`${sectionId}-${rowId}`}
        style={{
          height: 1,
          backgroundColor: '#ccc'
        }}
      />
    )
  };

  return (
    <View style={styles.container}>
      <AppHeader
        title={category.name}
        style={{backgroundColor:'#3498db'}}
        navigator={navigator}
      />
      <Image
        key="image"
        source={{uri: settings.serverURL + category.images[0].filename}}
        style={styles.backgroundImage}
      />
      <ScrollView
        automaticallyAdjustContentInsets={false}
        style={styles.fill}>
        <ListView
          style={styles.list}
          dataSource={ds.cloneWithRows(category.children)}
          renderRow={_renderRow}
          renderSeparator={_renderSeparator}
        />
      </ScrollView>
    </View>
  );

}

Category.propTypes = {
  category: PropTypes.object.isRequired
}

export default Category;
