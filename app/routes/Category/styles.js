import { create } from '../../common/AppStyleSheet';

export default create({
    container: {
      ios: {
        marginBottom: 50
      },
      flex: 1,
      backgroundColor: '#fff'
    },
    image: {
      width: null,
      height: 150
    },
    link: {
      paddingVertical: 8,
      justifyContent: 'center',
      paddingHorizontal: 10
    },
    linkContent: {
      flexDirection: 'row',
      alignItems: 'center'
    },
    linkText: {
      color: '#555',
      flexGrow: 1,
      fontFamily: 'cabin',
      fontSize: 16
    },
    icon: {
      tintColor: '#555'
    },
    header: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      backgroundColor: '#03A9F4',
      overflow: 'hidden',
    },
    bar: {
      marginTop: 28,
      height: 32,
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      backgroundColor: 'transparent',
      color: 'white',
      fontSize: 18
    },
    fill: {
      flex: 1,
    },
    backgroundImage: {
      width: null,
      height: 200,
      resizeMode: 'cover'
    }
});
