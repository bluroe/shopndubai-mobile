import React, { Component, PropTypes } from 'react';
import { ListView, Animated } from 'react-native';
import { connect } from 'react-redux';
import Category from './Category';
import ProductListing from '../../routes/ProductListing';
import settings from '../../config/settings';

class CategoryContainer extends Component {

  static propTypes = {
    category: PropTypes.object.isRequired
  }

  expand(category) {
    this.props.navigator.push({
      id: 'productListing',
      category
    });
  }

  componentDidMount() {
  }

  render() {
    return (
      <Category
        expand={this.expand.bind(this)}
        category={this.props.category}
        navigator={this.props.navigator}
        {...this.state}
      />
    );
  }

}

export default connect()(CategoryContainer);
