import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView
} from 'react-native';
import Button from '../../components/Button';
import AppHeader from '../../common/AppHeader';
import styles from './styles';


const Delivery = (props) => {

  const {
    onPress,
    firstname,
    lastname,
    address1,
    address2,
    city,
    phone,
    area,
    pin,
    setState,
    errorMessage,
    navigator
  } = props;

  const errors = errorMessage ? (
    <View style={{backgroundColor:'#f4424b',paddingHorizontal:10,paddingVertical:5}}>
      <Text style={{color:'#fff'}}>{errorMessage}</Text>
    </View>
  ) : null;

  return (
    <View style={styles.container}>
      <AppHeader
        title={"Delivery Address"}
        style={{backgroundColor:'#9b59b6'}}
        navigator={navigator}
      />
      {errors}
      <ScrollView ref={(view) => scrollView = view}>
        <View style={styles.content}>
          <View style={styles.row}>
            <View style={[styles.col, {paddingRight:5}]}>
              <Text style={styles.label}>First Name</Text>
              <TextInput style={styles.input}
                value={firstname}
                onChangeText={(firstname) => setState({firstname})}
                returnKeyType="next"
                onSubmitEditing={() => this.lastName.focus()}
                underlineColorAndroid="transparent" />
            </View>
            <View style={[styles.col, {paddingLeft:5}]}>
              <Text style={styles.label}>Last Name</Text>
              <TextInput style={styles.input}
                value={lastname}
                onChangeText={(lastname) => setState({lastname})}
                returnKeyType="next"
                ref={(input) => this.lastName = input}
                onSubmitEditing={() => this.address1.focus()}
                underlineColorAndroid="transparent" />
            </View>
          </View>
          <View>
            <Text style={styles.label}>Address Line 1</Text>
            <TextInput style={styles.input}
              value={address1}
              onChangeText={(address1) => setState({address1})}
              returnKeyType="next"
              ref={(input) => this.address1 = input}
              onSubmitEditing={() => this.address2.focus()}
              underlineColorAndroid="transparent" />
          </View>
          <View>
            <Text style={styles.label}>Address Line 2</Text>
            <TextInput style={styles.input}
              value={address2}
              onChangeText={(address2) => setState({address2})}
              returnKeyType="next"
              ref={(input) => this.address2 = input}
              onSubmitEditing={() => this.area.focus()}
              underlineColorAndroid="transparent" />
          </View>
          <View style={styles.row}>
            <View style={[styles.col, {paddingRight:5}]}>
              <Text style={styles.label}>Area</Text>
              <TextInput style={styles.input}
                value={area}
                onChangeText={(area) => setState({area})}
                returnKeyType="next"
                ref={(input) => this.area = input}
                onSubmitEditing={() => this.city.focus()}
                underlineColorAndroid="transparent" />
            </View>
            <View style={[styles.col, {paddingLeft:5}]}>
              <Text style={styles.label}>City</Text>
              <TextInput style={styles.input}
                value={city}
                onChangeText={(city) => setState({city})}
                returnKeyType="next"
                ref={(input) => this.city = input}
                onSubmitEditing={() => this.phone.focus()}
                underlineColorAndroid="transparent" />
            </View>
          </View>
          <View style={styles.row}>
            <View style={[styles.col, {paddingRight:5}]}>
              <Text style={styles.label}>Phone</Text>
              <TextInput style={styles.input}
                value={phone}
                onChangeText={(phone) => setState({phone})}
                keyboardType="phone-pad"
                returnKeyType="next"
                ref={(input) => this.phone = input}
                onSubmitEditing={() => this.pin.focus()}
                underlineColorAndroid="transparent" />
            </View>
            <View style={[styles.col, {paddingLeft:5}]}>
              <Text style={styles.label}>PIN</Text>
              <TextInput
                style={styles.input}
                value={pin}
                onChangeText={(pin) => setState({pin})}
                keyboardType="numeric"
                returnKeyType="done"
                ref={(input) => this.pin = input}
                onSubmitEditing={onPress}
                underlineColorAndroid="transparent" />
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={styles.buttonContainer}>
        <Button onPress={onPress} text="PLACE ORDER" />
      </View>
    </View>
  );

}

export default Delivery;
