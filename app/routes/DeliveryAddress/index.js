import DeliveryAddress from './DeliveryAddress';
import DeliveryAddressContainer from './DeliveryAddressContainer';

export { DeliveryAddress };
export default DeliveryAddressContainer;
