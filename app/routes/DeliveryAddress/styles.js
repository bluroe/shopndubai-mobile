import { create } from '../../common/AppStyleSheet';

export default create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    ios: {
      marginBottom: 50
    }
  },
  content: {
    padding: 15,
    flexGrow: 1
  },
  row: {
    flexDirection: 'row'
  },
  col: {
    flex: .5
  },
  label: {
    marginBottom: 5,
    color: '#555',
    fontFamily: 'cabin'
  },
  input: {
    height: 36,
    marginBottom: 5,
    color: '#555',
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    borderRadius: 18,
    borderColor: '#ccc',
    borderWidth: 1,
    android: {
      paddingTop: 0,
      paddingBottom: 0
    }
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    ios: {
      paddingVertical: 5
    },
    flexDirection: 'row',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowRadius: 3,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: .3,
    elevation: 2,
  },
});
