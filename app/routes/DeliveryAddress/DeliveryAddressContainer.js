import React from 'react';
import { connect } from 'react-redux';
import { editAddress, removeAddress } from '../../actions/customer';
import DeliveryAddress from './DeliveryAddress';
import OrderConfirmation from '../OrderConfirmation';

class DeliveryAddressContainer extends React.Component {

  constructor() {
    super();
    this.state = {
      firstname: '',
      lastname: '',
      address1: '',
      address2: '',
      phone: '',
      city: '',
      area: '',
      pin: '',
      errorMessage: ''
    };
    this.submitDeliveryAddress = this.submitDeliveryAddress.bind(this);
  }

  inputFocused (refName) {
    setTimeout(() => {
      let scrollResponder = this.refs.scrollView.getScrollResponder();
      scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
        React.findNodeHandle(this.refs[refName]),
        110, //additionalOffset
        true
      );
    }, 50);
  }

  componentDidMount() {
    // this.props.dispatch(removeAddress(0));
    let address = this.props.addresses[0];
    if(address) this.setState(address)
    else fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + this.props.geolocation)
      .then(response => response.json())
      .then(response => {
        console.log(response.results)
        if(response.results.length) {
          var collected = {};
          for(var result in response.results) {
            var address = response.results[result];
            if(address.types.indexOf('postal_code') > -1) {
              collected.pin = address.address_components[0].short_name;
            } else if(address.types.indexOf('locality') > -1) {
              collected.city = address.address_components[0].short_name;
            } else if(address.types.indexOf('sublocality') > -1) {
              collected.area = address.address_components[0].short_name;
            } else if(address.types.indexOf('route') > -1) {
              collected.address1 = address.address_components[0].short_name;
              collected.address2 = address.address_components[1].short_name;
            }
          }
          if(Object.keys(collected).length) this.setState(collected);
        }
      })
      .catch(error => console.log(error));
  }

  submitDeliveryAddress() {
    const {
      errorMessage,
      ...address
    } = this.state;
    const total = this.props.cart.reduce((carry, product) => product.price * product.qty, 0);
    const scene = {
      id: 'orderConfirmation',
      total,
      address
    }
    if(address.firstname === '') {
      this.setState({
        errorMessage: 'Please type first name'
      });
    } else if(address.lastname === '') {
      this.setState({
        errorMessage: 'Please type last name'
      });
    } else if(address.address1 === '') {
      this.setState({
        errorMessage: 'Please type address line 1'
      });
    } else if(address.phone === '') {
      this.setState({
        errorMessage: 'Please type phone number'
      });
    } else if(address.city === '') {
      this.setState({
        errorMessage: 'Please type city'
      });
    } else if(address.area === '') {
      this.setState({
        errorMessage: 'Please type area'
      });
    } else if(address.pin === '') {
      this.setState({
        errorMessage: 'Please type pin code'
      });
    } else if(address.pin.length != 6) {
      this.setState({
        errorMessage: 'Invalid PIN Code'
      });
    } else {
      this.setState({ errorMessage: '' })
      this.props.dispatch(editAddress(address, 0));
      this.props.navigator.push(scene);
    }
  }

  render() {
    return (
      <DeliveryAddress
        onPress={this.submitDeliveryAddress}
        {...this.state}
        setState={(state) => this.setState(state)}
        navigator={this.props.navigator}
      />
    )
  }

}

const mapStates = store => ({
  addresses: store.customer.addresses,
  cart: store.cart.items,
  geolocation: store.location.position
});

export default connect(mapStates)(DeliveryAddressContainer);
