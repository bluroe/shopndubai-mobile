import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import Search from './Search';
import settings from '../../config/settings';

class SearchContainer extends Component {

  constructor(props) {
    super();
    this.state = {
      results: [],
      loading: false,
      hasFocus: false,
      search: ''
    };
    this.cancelSearch = this.cancelSearch.bind(this);
  }

  componentDidMount() {
  }

  componentWillUnmount() {
    this.cancelSearch();
  }

  cancelSearch() {
    if(this.cancel) {
      this.cancel();
    }
  }

  loadItems(text) {
    this.setState({
      loading: true,
      search: text
    });
    this.cancelSearch();
    if(text) {
      const query = `
        {
          products(name:"${text}") {
            items {
              id,
              name,
              variants {
                id,
                type,
                price
                offer {
                  text,
                  price,
                  add_qty,
                  qty
                }
              },
              images {
                filename
              }
            },
            total
          }
        }
      `;
      const url = settings.serverURL + '/graphql?query=' + query;
      // console.log(url);
      axios.get(url, {
        cancelToken: new axios.CancelToken((c) => {
          this.cancel = c;
        })
      })
      .then((response) => {
        const responseJson = response.data;
        const products = responseJson.data.products.items;
        this.setState({
          results: products,
          loading: false
        });
        // console.log('search `' + text + '` : ' + products.length + ' results');
      })
      .catch((error) => {
        if (error.response) {
          // The request was made, but the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else {
          // Something happened in setting up the request that triggered an Error
          // console.log('Error', error.message);
        }
        // console.log(error.config);
      });
      // fetch(url)
      //   .then((response) => response.json())
      //   .then((responseJson) => {
      //     const products = responseJson.data.products.items;
      //     this.setState({
      //       results: products,
      //       loading: false
      //     });
      //     console.log('search `' + text + '` : ' + products.length + ' results');
      //   })
      //   .catch((error) => console.log(error));
    } else {
      this.setState({
        results: [],
        loading: false
      });
    }
  }

  openProduct(product) {
    this.props.navigator.push({
      id: 'productView',
      product
    })
  }

  render() {
    return (
      <Search
        loadItems={this.loadItems.bind(this)}
        openProduct={this.openProduct.bind(this)}
        navigator={this.props.navigator}
        {...this.state}
      />
    );
  }

}

const states = (store) => ({
  geolocation: store.location.position
})

export default connect(states)(SearchContainer);
