import { create } from '../../common/AppStyleSheet';
import { Platform } from 'react-native';

export default create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    ios: {
      marginBottom: 50
    }
  },
  inputContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    elevation: 2,
    shadowOpacity: .3,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowColor: '#000',
    shadowRadius: 3,
    height: Platform.OS === 'ios' ? 75 : 50,
    ios: {
      paddingTop: 25,
      zIndex: 10
    },
  },
  input: {
    flex: 1,
    fontFamily: 'cabin'
  },
  list: {
    backgroundColor: '#fff',
  },
  backBtn: {
    justifyContent:'center',
    alignItems:'center',
    paddingLeft: 10,
    paddingVertical: 10,
    paddingRight: 15,
  },
  row: {
    flexDirection:'row',
    padding:5,
    backgroundColor:'#fff'
  },
  rowText: {
    color: '#555',
    padding: 10,
    backgroundColor: '#fff',
    fontFamily: 'cabin',
    flexGrow: 1
  },
  emptyList: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  messageIcon: {
    tintColor: '#999',
    marginBottom: 10
  },
  messageText: {
    fontSize: 16,
    color: '#555',
    fontFamily: 'cabin'
  }
});
