import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  ListView,
  Image
} from 'react-native';
import _ from 'lodash';
import AppHeader from '../../common/AppHeader';
import styles from './styles';
import images from '../../config/images';
import settings from '../../config/settings';
import Loading from '../../components/Loading';

const Search = (props) => {

  const { results, loadItems, openProduct, navigator, loading, search } = props;

  const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

  let products = (
    <View style={styles.emptyList}>
      <Image source={images.search_empty} style={styles.messageIcon} />
      <Text style={styles.messageText}>{search.length ? 'No results' : 'Type to search'}</Text>
    </View>
  );
  if(loading) {
    products = <Loading style={{backgroundColor:'#fff'}} />;
  } else if(results.length) {
    products = (
      <ListView
        style={styles.list}
        dataSource={ds.cloneWithRows(results)}
        automaticallyAdjustContentInsets={false}
        renderRow={(product) => {
          const image = (product.images && product.images.length)
            ? {uri: settings.serverURL + product.images[0].filename}
            : images.noimage;
          return (
            <TouchableHighlight onPress={openProduct.bind(this, product)} style={styles.item}>
              <View style={styles.row}>
                <Image source={image} style={{width:40,height:40}} resizeMode="contain" />
                <Text style={styles.rowText}>{product.name}</Text>
              </View>
            </TouchableHighlight>
          );
        }}
        renderSeparator={(section, row) => {
          return (
            <View
              key={row}
              style={{
                height: 1,
                backgroundColor: '#ccc',
              }}
            />
          )
        }}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <TouchableOpacity style={styles.backBtn} onPress={() => navigator.pop()}>
          <Image source={images.chevronLeft} style={{width:16,height:16}} resizeMode="contain" />
        </TouchableOpacity>
        <TextInput
          style={styles.input}
          onChangeText={_.debounce(loadItems, 250)}
          placeholder="Search"
          underlineColorAndroid="transparent"
        />
      </View>
      {products}
    </View>
  );

};

export default Search;
