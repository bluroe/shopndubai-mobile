import React, { Component } from 'react';
import { Alert } from 'react-native';
import { connect } from 'react-redux';
import OrderConfirmation from './OrderConfirmation';
import { clearCart } from '../../actions/cart';
import { loadOrders } from '../../actions/orders';
import { setKey } from '../../actions/customer';
import settings from '../../config/settings';

class OrderConfirmationContainer extends Component {

  constructor() {
    super();
    this.placeOrder = this.placeOrder.bind(this);
    this.state = {
      loading: false,
      error: false
    }
  }

  componentDidMount() {
  }

  _placeOrder(force = 0) {
    this.setState({loading: true});
    const address = this.props.addresses[0];
    const data = JSON.stringify({
      items: this.props.cart,
      key: this.props.customerKey,
      address,
      force
    });
    // console.log(data)
    fetch(settings.apiURL + 'order', {
      method: 'POST',
      body: data
    })
    .then((response) => {
      if(response.ok) {
        response.json().then(rsp => {
          console.log(rsp)
          if(rsp.status == 'no_delivery') {
            Alert.alert(
              rsp.title,
              rsp.msg,
              [
                {text: 'Cancel', onPress: () => this.setState({ loading: false, error: false }), style: 'cancel'},
                {text: 'OK', onPress: () => this._placeOrder(1)},
              ],
              { cancelable: false }
            );
            return;
          }
          if(rsp.status == 'customer_not_found') {
            Alert.alert(
              rsp.title,
              rsp.msg,
              [
                {text: 'OK', onPress: () => this.props.dispatch(setKey(''))},
              ],
              { cancelable: false }
            );
            return;
          }
          this.setState({ loading: false, error: false });
          this.props.dispatch(clearCart());
          this.props.dispatch(loadOrders());
          this.props.navigator.push({id: 'orderPlaced', msg: rsp.msg});
        })
      } else {
        response.text().then(responseTxt => {
          console.log(responseTxt)
          this.setState({ error: true, loading: false });
        })
      }
    })
  }

  placeOrder(event) {
    this._placeOrder();
  }

  render() {
    let total = this.props.cart.reduce((carry, product) => carry + (product.price * product.qty), 0);
    total += this.props.deliveryCharge;
    const address = this.props.addresses[0];
    return (
      <OrderConfirmation
        placeOrder={this.placeOrder}
        total={total}
        address={address}
        loading={this.state.loading}
        error={this.state.error}
        navigator={this.props.navigator}
      />
    );
  }

}

const select = (store) => ({
  cart: store.cart.items,
  addresses: store.customer.addresses,
  customerKey: store.customer.key,
  deliveryCharge: store.cart.deliveryCharge
});

export default connect(select)(OrderConfirmationContainer);
