import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';
import Button from '../../components/Button';
import AppHeader from '../../common/AppHeader';
import Loading from '../../components/Loading';
import settings from '../../config/settings';
import styles from './styles';
import images from '../../config/images';
import ErrorPage from '../../common/ErrorPage';

const OrderConfirmation = (props) => {

  const { total, address, placeOrder, loading, navigator, error } = props;

  let content;
  if(error && !loading) {
    content = <ErrorPage onPress={placeOrder} />;
  } else if(loading) {
    content = <Loading />;
  } else {
    content = (
      <View style={{flexGrow:1}}>
        <View style={styles.content}>
          <View style={styles.box}>
            <View style={styles.title}>
              <Text style={[styles.headText, {marginBottom: 10}]}>Order Total : {total} {settings.priceText}</Text>
            </View>
            <Text style={[styles.headText, {marginBottom: 10}]}>Delivery Address:</Text>
            <Text style={styles.contentText}>{address.firstname + ' ' + address.lastname}</Text>
            <Text style={styles.contentText}>{address.address1}</Text>
            <Text style={styles.contentText}>{address.area + ' ' + address.city}</Text>
            <Text style={styles.contentText}>{address.phone}</Text>
            <Text style={styles.contentText}>{address.pin}</Text>
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button onPress={placeOrder} text="CONFIRM ORDER" />
        </View>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <AppHeader
        title={"Order Confirmation"}
        style={{backgroundColor:'#34495e'}}
        navigator={navigator}
      />
      {content}
    </View>
  );
}

export default OrderConfirmation;
