import { create } from '../../common/AppStyleSheet';
import { button, buttonText } from '../../common/styles';

export default create({
  container: {
    backgroundColor: '#fafafa',
    flexDirection: 'column',
    flex: 1,
    ios: {
      marginBottom: 50
    }
  },
  content: {
    padding: 15,
    flexGrow: 1
  },
  box: {
    padding: 20,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0
    },
    elevation: 4,
    backgroundColor: '#fff'
  },
  title: {
    borderBottomWidth: 1,
    borderColor: '#ccc',
    marginBottom: 10
  },
  headText: {
    fontSize: 20,
    color: '#555',
    fontFamily: 'cabin'
  },
  contentText: {
    color: '#555',
    fontFamily: 'cabin',
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    ios: {
      paddingVertical: 5
    },
    flexDirection: 'row',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowRadius: 3,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: .3,
    elevation: 2
  }
});
