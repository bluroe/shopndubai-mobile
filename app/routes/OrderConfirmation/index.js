import OrderConfirmation from './OrderConfirmation';
import OrderConfirmationContainer from './OrderConfirmationContainer';

export { OrderConfirmation };
export default OrderConfirmationContainer;
