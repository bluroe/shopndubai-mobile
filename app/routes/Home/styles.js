import { create } from '../../common/AppStyleSheet';
import { Platform } from 'react-native';

export default create({
  toolbar: {
    height: 65,
    backgroundColor: '#ccc'
  },
  container: {
    backgroundColor: '#ccc',
    ios: {
      marginBottom: 50
    }
  },
  contentContainer: {
  },
  row: {
    flexDirection: 'row'
  },
  imgSlider: {
    backgroundColor: '#fafafa',
    shadowOffset: {
      height: 3,
      width: 0
    },
    shadowColor: '#000',
    shadowRadius: 3,
    shadowOpacity: .1,
    elevation: 2,
    marginBottom: 10
  },
  bannerContainer: {
    height: 190,
    justifyContent: 'center',
  },
  bannerImage: {
    width: null,
    height: 190
  }
});
