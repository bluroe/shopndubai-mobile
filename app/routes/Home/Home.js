import React from 'react';
import {
  View,
  ListView,
  Text,
  TouchableHighlight,
  Image,
  ScrollView,
  RefreshControl
} from 'react-native';
import Swiper from 'react-native-swiper';
import AppHeader from '../../common/AppHeader';
import images from '../../config/images';
import HomeTile from '../../components/HomeTile';
import Loading from '../../components/Loading';
import styles from './styles';
import settings from '../../config/settings';
import ErrorPage from '../../common/ErrorPage';

const Home = (props) => {

  const {
    loading,
    categories,
    openCategory,
    openSearch,
    banners,
    error,
    loadCats,
    ds
  } = props;

  const renderSeparator = (sectionId, rowId, adjacentRowHighlighted) => {
    return (
      <View
        key={`${sectionId}-${rowId}`}
        style={{
          height: 1,
          backgroundColor: '#fff'
        }}
      />
    );
  };

  let banner_items = null;
  if(banners.length) {
    const slides = banners.map((banner, key) => (
      <View style={styles.bannerContainer} key={key}>
        <Image source={{uri: settings.serverURL + banner}} style={styles.bannerImage} resizeMode="cover"/>
      </View>
    ))
    banner_items = (
      <View style={styles.imgSlider}>
        <Swiper
          height={190}
          autoplay={true}
          showsPagination={false}
          autoplayTimeout={5}>
          {slides}
        </Swiper>
      </View>
    )
  }

  const _renderRow = (category) => {
    return <HomeTile category={category} onPress={openCategory.bind(this, category)} />
  }

  const rightItem = {
    title: 'Search',
    icon: images.search,
    onPress: openSearch
  }

  if(loading && categories.length == 0) {
    content = <Loading />;
  } else if(error) {
    content = <ErrorPage onPress={loadCats} />;
  } else {
    const refreshControl = (
      <RefreshControl
        refreshing={loading}
        onRefresh={loadCats}
      />
    )
    content = (
      <ScrollView
        refreshControl={refreshControl}
      >
      {banner_items}
      <ListView
        style={styles.container}
        dataSource={ds.cloneWithRows(categories)}
        renderRow={_renderRow}
        renderSeparator={renderSeparator}
      />
      </ScrollView>
    );
  }

  return (
    <View style={{flex:1}}>
      <AppHeader
        logo={images.telystrip}
        rightItem={rightItem}
        foreground="dark"
      />
      {content}
    </View>
  );

}

export default Home;
