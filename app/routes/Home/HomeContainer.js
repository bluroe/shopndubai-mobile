import React, { Component, PropTypes } from 'react';
import { ListView } from 'react-native';
import Home from './Home';
import Category from '../../routes/Category';
import ProductListing from '../../routes/ProductListing';
import settings from '../../config/settings';
import axios from 'axios';

class HomeContainer extends Component {

  constructor() {
    super();
    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.state = {
      categories: [],
      loading: true,
      banners: [],
      error: false
    };
    this.loadCats = this.loadCats.bind(this);
  }

  componentDidMount() {
    this.loadCats();
    fetch(settings.apiURL + 'banners')
      .then((response) => response.json())
      .then((response) => {
        this.setState({banners:response});
      })
  }

  componentWillUnmount() {
    this.cancel();
  }

  loadCats() {
    this.setState({
      error: false,
      loading: true
    });
    const query = `
      {
        categories(depth:0) {
          images {
            filename
          },
          name,
          id,
          children {
            name,
            id
          }
        }
      }
    `;
    const url = settings.serverURL + '/graphql?query=' + query;
    // console.log(url)
    axios.get(url, {
      cancelToken: new axios.CancelToken((c) => {
        this.cancel = c;
      })
    })
      .then((rsp) => {
        const json = rsp.data.data;
        console.log('loading ' + json.categories.length + ' categories');
        this.setState({
          categories: json.categories,
          loading: false
        });
      })
      .catch((error) => {
        this.setState({
          error: true,
          loading: false
        })
        if (error.response) {
          // The request was made, but the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else {
          // Something happened in setting up the request that triggered an Error
          // console.log('Error', error.message);
        }
        // console.log(error.config);
      });
  }

  openCategory(category) {
    if(category.children.length) {
      this.props.navigator.push({id: 'category', category})
    } else {
      this.props.navigator.push({id: 'productListing', category});
    }
  }

  openSearch() {
    this.props.navigator.push({id:'search'})
  }

  render() {
    return (
      <Home
        openCategory={this.openCategory.bind(this)}
        openSearch={this.openSearch.bind(this)}
        loadCats={this.loadCats}
        ds={this.ds}
        {...this.state}
      />
    );
  }

}


export default HomeContainer;
