import React, { Component, PropTypes } from 'react';
import OrderPlaced from './OrderPlaced';
import { connect } from 'react-redux';
import { switchTab } from '../../actions/navigation';

class OrderPlacedContainer extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
  }

  gotoHome() {
    this.props.dispatch(switchTab('home'));
    this.props.navigator.popToTop();
  }

  gotoOrders() {
    this.props.dispatch(switchTab('orders'));
    this.props.navigator.popToTop();
  }

  render() {
    return (
      <OrderPlaced
        gotoHome={this.gotoHome.bind(this)}
        gotoOrders={this.gotoOrders.bind(this)}
        msg={this.props.msg}
      />
    );
  }

}

export default connect()(OrderPlacedContainer);
