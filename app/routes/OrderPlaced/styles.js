import { create } from '../../common/AppStyleSheet';
import { Dimensions } from 'react-native';
import { button, buttonText } from '../../common/styles';

export default create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    ios: {
      marginBottom: 50
    }
  },
  content: {
    padding: 20,
  },
  card: {
    top: -60,
    backgroundColor: '#fff',
    paddingTop: 20,
    paddingBottom: 30,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowColor: '#000',
    borderRadius: 4,
    shadowOffset: {
      width: 0,
      height: 0
    },
    elevation: 4
  },
  image: {
    width: null,
    height: Dimensions.get('window').height / 2,
  },
  headText: {
    color: '#555',
    fontSize: 20,
    marginBottom: 10,
    textAlign: 'center',
    fontFamily: 'cabin'
  },
  contentText: {
    color: '#999',
    textAlign: 'center',
    fontFamily: 'cabin'
  },
  buttonContainer: {
    paddingBottom: 20
  },
  button,
  buttonText,
  bg: {
    width: Dimensions.get('window').width,
    height: 260,
    resizeMode: 'cover'
  },
  icon: {
    alignSelf: 'center',
    marginBottom: 20
  }
});
