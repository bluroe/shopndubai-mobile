import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableHighlight
} from 'react-native';
import AppHeader from '../../common/AppHeader';
import Button from '../../components/Button';
import images from '../../config/images';
import styles from './styles';

const OrderPlaced = (props) => {

  const { gotoHome, gotoOrders, msg } = props;

  const msgs = typeof msg === 'string' ?
    <Text style={styles.contentText}>{m}</Text> :
    (
      <View>
        {msg.map((m, i) => (
          <Text style={styles.contentText} key={i}>{m}</Text>
        ))}
      </View>
    );

  return (
    <View style={styles.container}>
      <AppHeader
        title={"Order Placed"}
        style={{backgroundColor:'#28d674'}}
      />
      <Image source={images.bg} style={styles.bg} />
      <View style={styles.content}>
        <View style={styles.card}>
          <Text style={styles.headText}>We are on our way!</Text>
          {msgs}
          <Text style={[styles.contentText, {marginBottom:10}]}>Thank you for shopping with</Text>
          <Image source={images.telystrip} style={styles.icon} />
          <Button onPress={gotoOrders} text="MANAGE ORDERS" style={{marginBottom:10,alignSelf:'center'}} />
          <TouchableHighlight onPress={gotoHome} style={[styles.button, styles.buttonWhite]} underlayColor="#e25a3f">
            <Text style={styles.buttonText}>CONTINUE</Text>
          </TouchableHighlight>
        </View>
      </View>
    </View>
  );

};

export default OrderPlaced;
