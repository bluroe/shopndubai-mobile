import React from 'react';
import { Platform } from 'react-native';
import { connect } from 'react-redux';
import { setKey, getCustomerKey } from './actions/customer';
import Navigation from './components/Navigation';
import MyScene from './MyScene';
import RegisterPage from './common/RegisterPage';
import Loading from './components/Loading';

class App extends React.Component {

  componentDidMount() {
    // this.props.dispatch(setKey('')) // reset key
    // if(this.props.customerKey == '') {
    //   this.props.dispatch(getCustomerKey());
    // }
  }

  render() {
    if(this.props.customerKey == '') {
      // return <Loading />;
      return <RegisterPage />;
    }
    // return <MyScene text="moozie" />
    return <Navigation />;
  }

}

const select = (store) => ({
  customerKey: store.customer.key
});

export default connect(select)(App);
