export const colors = {
  accentBtnBg: "#65D165",
  primaryBtnBg: '#FF6749',
  primaryBtnBgDark: '#e25a3f',
  dangerColor: '#FF4136'
}
