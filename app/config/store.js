import { compose, createStore, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import { AsyncStorage } from 'react-native';
import createLogger from 'redux-logger';

const logger = createLogger({
  predicate: () => process.env.NODE_ENV === 'development'
});

const middleWare = [ thunk, logger ];

const createStoreWithMiddleware = applyMiddleware(...middleWare)(createStore);

export function makeStore(onComplete :? () => void) {
  const store = autoRehydrate()(createStoreWithMiddleware)(reducers);
  persistStore(store, {
    storage: AsyncStorage,
    blacklist: ['orders']
  }, onComplete)
  // .purge();
  return store;
}

export default makeStore;
