import React from 'react';

import Home from '../routes/Home';
import Search from '../routes/Search';
import Category from '../routes/Category';
import DeliveryAddress from '../routes/DeliveryAddress';
import OrderConfirmation from '../routes/OrderConfirmation';
import OrderPlaced from '../routes/OrderPlaced';
import OrderView from '../routes/OrderView';
import ProductListing from '../routes/ProductListing';
import ProductView from '../routes/ProductView';
import Register from '../routes/Register';
import Login from '../routes/Login';

export const routes = {
  getHomeRoute() {
    return {
      renderScene(navigator) {
        return <Home navigator={navigator} />;
      },
      getTitle() {
        return 'Home';
      }
    }
  },
  getCategoryRoute() {
    return {
      return {
        renderScene(navigator) {
          return <Category navigator={navigator} />;
        },
        getTitle() {
          return 'Category';
        }
      }
    }
  }
}

export default routes;
