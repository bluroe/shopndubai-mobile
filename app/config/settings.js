const serverURL = 'https://telybazaar.com';

const settings = {
  priceText: 'RS',
  apiURL: serverURL + '/api/',
  serverURL
}

export default settings;
