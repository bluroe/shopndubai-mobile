const images = {
  whitebox: require('../images/empty-white-box.png'),
  noimage: require('../images/no-image.png'),
  telystrip: require('../images/tely_strip.png'), // logo
  telyvert: require('../images/tely_vert.png'), // logo
  telyicon: require('../images/tely_icon.png'), // logo
  bag: require('../images/bag.png'),
  warning: require('../images/warning.png'),
  bg: require('../images/bg3.png'),
  search_empty: require('../images/search.png'),
  empty: require('../images/miscellaneus.png'),
  checked: require('../images/checked.png'),
  /* icons */
  search: require('../images/icons/search.png'),
  search_white: require('../images/icons/search_white.png'),
  bars: require('../images/icons/bars.png'),
  bars_white: require('../images/icons/bars_white.png'),
  chevronLeft: require('../images/icons/chevron-left.png'),
  chevronRight: require('../images/icons/chevron_right.png'),
  list: require('../images/icons/list.png'),
  cart: require('../images/icons/cart.png'),
  cart_white: require('../images/icons/cart_white.png'),
  cart_white_f: require('../images/icons/cart_f.png'),
  home: require('../images/icons/home.png'),
  add: require('../images/icons/add.png'),
  remove: require('../images/icons/remove.png'),
  briefcase: require('../images/icons/briefcase.png'),
  caret: require('../images/icons/caret-down.png'),
}

export default images;
