import settings from '../config/settings';
import axios from 'axios';

export function setOrders(orders) {
  return {
    type: 'SET_ORDERS',
    orders
  }
}

export function loadOrders() {
  return (dispatch, getState) => {
    var { key } = getState().customer;
    const query = `
      {
        orders(customer:"${key}") {
          id,
          status,
          total,
          delivery_charge,
          orderItems {
            name,
            price,
            qty,
            image
          }
        }
      }
    `;
    const url = settings.serverURL + '/graphql?query=' + query;
    return axios.get(url)
    .then((rsp) => {
        const json = rsp.data;
        dispatch(setOrders(json.data.orders));
    })
    .catch((error) => {
        if (error.response) {
          // The request was made, but the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else {
          // Something happened in setting up the request that triggered an Error
          // console.log('Error', error.message);
        }
        // console.log(error.config);
    });
  }
}
