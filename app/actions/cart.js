import settings from '../config/settings';
import axios from 'axios';

export function addToCart(product, variant, qty) {
  return {
    type: 'ADD_ITEM',
    product,
    variant,
    qty
  }
}

export function removeItem(id) {
  return {
    type: 'REMOVE_ITEM',
    id
  }
}

export function removeItems(items) {
  return {
    type: 'REMOVE_ITEMS',
    items
  }
}

export function clearCart() {
  return {
    type: 'CLEAR'
  }
}

export function setDeliveryCharge(amount) {
  return {
    type: 'SET_DELIVERY_CHARGE',
    amount
  }
}

export function updateDeliveryCharge() {
  return (dispatch, getState) => {
      const items = {};
      for(let item of getState().cart.items) {
        items[item.id] = item.qty;
      }
      return axios.post(settings.apiURL + 'delivery-charge', {
        items
      })
      .then((rsp) => {
        console.log(rsp.data);
        if(rsp.data.status == 'ok') {
          const delivery_charge = rsp.data.delivery_charge;
          dispatch(setDeliveryCharge(delivery_charge));
        }
      })
      .catch((error) => {
          if (error.response) {
            // The request was made, but the server responded with a status code
            // that falls out of the range of 2xx
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          } else {
            // Something happened in setting up the request that triggered an Error
            // console.log('Error', error.message);
          }
          // console.log(error.config);
      });
  }
}
