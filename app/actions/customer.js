import settings from '../config/settings';
import { setActive, setPin, setStores } from './location';
import axios from 'axios';

export function setKey(key) {
  return {
    type: 'SET_KEY',
    key
  }
}

function setKeyError() {
  return {
    type: 'SET_KEY_ERROR'
  }
}

export function addAddress(address) {
  return {
    type: 'ADD_ADDRESS',
    address
  }
}

export function editAddress(address, index) {
  return {
    type: 'EDIT_ADDRESS',
    address,
    index
  }
}

export function removeAddress(index) {
  return {
    type: 'REMOVE_ADDRESS',
    index
  }
}

export function updatePin(callback) {
  return function(dispatch) {
    return fetch(settings.apiURL + 'update-pin')
      .then((response) => response.json())
      .then((response) => {
        dispatch(setStores(response.stores));
        dispatch(setActive(response.stores > 0));
        if(typeof callback == 'function') callback();
      })
      .catch((error) => dispatch(setKeyError()));
  }
}

export function getCustomerKey(callback) {
  return function(dispatch) {
    return axios.get(settings.apiURL + 'register')
      .then((rsp) => {
        rsp = rsp.data;
        dispatch(setKey(rsp.key));
        if(typeof callback == 'function') callback();
      })
      .catch((error) => dispatch(setKeyError()));
  }
}

export function requestOtp(mobile) {
  return (dispatch) => {
    const url = settings.apiURL + 'request-otp';
    return axios.post(url, {
      mobile
    });
  }
}

export function sendOtp(otp, mobile) {
  return (dispatch) => {
    const url = settings.apiURL + 'send-otp';
    return axios.post(url, {
      mobile,
      otp
    });
  }
}
