function setPin(pin) {
  return {
    type: 'SET_PIN',
    pin
  }
}

export function setActive(active) {
  return {
    type: 'SET_ACTIVE',
    active
  }
}

export function setStores(stores) {
  return {
    type: 'SET_STORES',
    stores
  }
}
