import React from 'react';
import {
  TouchableWithoutFeedback,
  View,
  Animated,
  Image,
  Text,
  Platform,
  StyleSheet,
  Dimensions
} from 'react-native';
import images from '../config/images';

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? 44 + STATUS_BAR_HEIGHT : 56 + STATUS_BAR_HEIGHT;
const TAB_BAR_HEIGHT = Platform.OS === 'ios' ? 50 : 0;
const REDUCE_HEIGHT = HEADER_HEIGHT + TAB_BAR_HEIGHT;
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  modal: {
    width,
    backgroundColor: 'rgba(0,0,0,.3)',
    position: 'absolute',
    height: height - REDUCE_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    top: HEADER_HEIGHT,
    zIndex: 10
  },
  modalBox: {
    width: 200,
    height: 200,
    backgroundColor: '#fff',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalImage: {
    marginBottom: 20,
    width: 60,
    height: 60
  },
  modalText: {
    fontFamily: 'cabin',
    color: '#555',
    fontSize: 16
  }
})

class CartModal extends React.Component {

  constructor() {
    super();
    this.state = {
      show: false,
      fadeAnim: new Animated.Value(.5),
    }
  }

  componentWillUnmount() {
    this.closeModal();
  }

  showModal() {
    this.setState({
      show: true
    });
    Animated.spring(this.state.fadeAnim, {
      toValue: 1,
      tension: 80
    }).start();
    this.timeout = setTimeout(() => {
      this.setState({show: false});
      this.state.fadeAnim.setValue(.5);
    }, 2000);
  }

  closeModal() {
    this.setState({show: false});
    this.state.fadeAnim.setValue(.5);
    clearTimeout(this.timeout);
  }

  render() {
    if(!this.state.show) return null;
    return (
      <TouchableWithoutFeedback onPress={this.closeModal.bind(this)}>
        <View style={styles.modal}>
          <Animated.View style={[styles.modalBox, {transform:[{scale: this.state.fadeAnim}]}]}>
            <Image source={images.checked} style={styles.modalImage} />
            <Text style={styles.modalText}>Added to cart</Text>
          </Animated.View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default CartModal;
