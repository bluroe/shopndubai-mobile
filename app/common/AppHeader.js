import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Platform,
  StyleSheet,
  ToolbarAndroid
} from 'react-native';
import images from '../config/images';

class HeaderAndroid extends React.Component {

  static contextTypes = {
    openDrawer: React.PropTypes.func
  }

  handleActionSelected(position) {
    let items = this.props.extraItems || [];
    if(this.props.rightItem) {
      items = [this.props.rightItem, ...items];
    }
    const item = items[position];
    item && item.onPress && item.onPress();
  }

  render() {
    const {rightItem, extraItems, foreground } = this.props;
    let leftItem = this.props.leftItem;
    if(!leftItem) {
      leftItem = {
        title: 'Menu',
        icon: foreground === 'dark' ? images.bars : images.bars_white,
        onPress: () => this.context.openDrawer()
      }
    }

    let actions = [];
    if(rightItem) {
      const {title, icon, layout} = rightItem;
      actions.push({
        icon: layout !== 'title' ? icon : undefined,
        title: title,
        show: 'always'
      });
    }
    if(extraItems) {
      actions = actions.concat(extraItems.map((item) => ({
        title: item.title,
        icon: item.icon,
        show: 'always'
      })))
    }

    const textColor = foreground === 'dark' ? '#555' : '#fff';

    return (
      <View style={this.props.style}>
        <ToolbarAndroid
          logo={this.props.logo}
          navIcon={leftItem.icon}
          onIconClicked={leftItem && leftItem.onPress}
          title={this.props.title}
          titleColor={textColor}
          actions={actions}
          onActionSelected={this.handleActionSelected.bind(this)}
          style={styles.toolbar}
        />
      </View>
    )
  }

}

class HeaderIOS extends React.Component {

  render() {
    const {title, logo, rightItem, navigator, foreground} = this.props;
    let leftItem = this.props.leftItem;
    if(!leftItem && navigator) {
      leftItem = {
        title: 'Back',
        icon: images.chevronLeft,
        onPress: () => navigator.pop()
      }
    }

    const titleColor = foreground === 'dark' ? '#555' : '#fff';
    const itemsColor = foreground === 'dark' ? '#555' : '#fff';

    const content = title
      ? <Text style={[styles.titleText, {color:titleColor}]} numberOfLines={1}>
          {title}
        </Text>
      : <Image source={logo} />;

    return (
      <View style={[styles.header, this.props.style]}>
        <View style={styles.leftItem}>
          <ItemWrapperIOS color={itemsColor} item={leftItem} />
        </View>
        <View style={styles.centerItem}>
          {content}
        </View>
        <View style={styles.rightItem}>
          <ItemWrapperIOS color={itemsColor} item={rightItem} />
        </View>
      </View>
    )
  }
}

class ItemWrapperIOS extends React.Component {

  render() {
    const {item, color} = this.props;
    if(!item) {
      return null;
    }

    let content;
    const {title, icon, layout, onPress} = item;

    if(layout !== 'icon' && title) {
      content = (
        <Text style={[styles.itemText, {color}]}>
          {title.toUpperCase()}
        </Text>
      )
    } else if(icon) {
      content = <Image source={icon} />;
    }

    return (
      <TouchableOpacity
        onPress={onPress}
        style={styles.itemWrapper}>
        {content}
      </TouchableOpacity>
    );
  }

}

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : 25;
const HEADER_HEIGHT = Platform.OS === 'ios' ? 44 + STATUS_BAR_HEIGHT : 56 + STATUS_BAR_HEIGHT;

const styles = StyleSheet.create({
  toolbar: {
    height: HEADER_HEIGHT - STATUS_BAR_HEIGHT
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: STATUS_BAR_HEIGHT,
    height: HEADER_HEIGHT
  },
  leftItem: {
    flex: 1,
    alignItems: 'flex-start'
  },
  centerItem: {
    flex: 2,
    alignItems: 'center'
  },
  rightItem: {
    flex: 1,
    alignItems: 'flex-end'
  },
  itemWrapper: {
    padding: 11
  },
  itemText: {
    color: '#fff',
    fontSize: 12,
    fontFamily: 'cabin'
  },
  titleText: {
    fontFamily: 'cabin',
  }
});

const Header = Platform.OS === 'ios' ? HeaderIOS : HeaderAndroid;

export default Header;
