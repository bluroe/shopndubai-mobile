import React from 'react';
import {
  ListView,
  Text,
  View,
  Image
} from 'react-native';
import images from '../config/images';

const styles = {
  container: {
    marginTop: 8,
    backgroundColor: '#fff',
    flex: 1,
  },
  list: {
  },
  row: {
    flexDirection:'row',
    padding:5,
    backgroundColor:'#fff'
  },
  rowText: {
    color: '#555',
    padding: 10,
    backgroundColor: '#fff'
  },
  thumb: {
    width:40,
    height:40
  },
  divider: {
    height: 1,
    backgroundColor: '#ccc'
  },
  emptyList: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  messageIcon: {
    tintColor: '#999',
    marginBottom: 10
  },
  messageText: {
    fontSize: 16,
    color: '#555',
    fontFamily: 'cabin'
  }
}

class SearchList extends React.Component {

  constructor() {
    super();
    this.state = {
      ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    }
  }

  _renderRow(product) {
    const image = (product.images && product.images.length)
      ? {uri: settings.serverURL + product.images[0].filename}
      : images.noimage;
    return (
      <TouchableHighlight onPress={openProduct.bind(this, product)}>
        <View style={styles.row}>
          <Image source={image} style={styles.thumb} resizeMode="contain" />
          <Text style={styles.rowText}>{product.name}</Text>
        </View>
      </TouchableHighlight>
    );
  }

  search(key) {
    const query = `
      {
        products(name:${key}) {
          items {
            id,
            name
          },
          total
        }
      }
    `;
    fetch(settings.serverURL + '/graphql?query=' + query)
      .then((response) => response.json())
      .then((responseJson) => {
        var products = responseJson.data.products.items;
        this.setState({
          results: products,
          loading: false
        });
        console.log('search `' + key + '` : ' + products.length + ' results');
      })
      .catch((error) => console.log(error));
  }

  render() {
    let content = null;
    if(this.props.products.length) {
      content = (
        <ListView
          style={styles.list}
          dataSource={this.state.ds.cloneWithRows(this.props.products)}
          automaticallyAdjustContentInsets={false}
          renderRow={this._renderRow}
          renderSeparator={(section, row) => {
            return (
              <View
                key={row}
                style={styles.divider}
              />
            )
          }}
        />
      );
    } else {
      content = (
        <View style={styles.emptyList}>
          <Image source={images.search_empty} style={styles.messageIcon} />
          <Text style={styles.messageText}>No results</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {content}
      </View>
    )
  }

}

export default SearchList;
