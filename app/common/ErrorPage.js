import React from 'react';
import { button, buttonText } from './styles';
import {
  View,
  Image,
  Text,
  TouchableHighlight,
  StyleSheet
} from 'react-native';
import images from '../config/images';

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  messageIcon: {
    width: 100,
    height: 100,
    marginBottom: 10,
    tintColor: '#555'
  },
  messageText: {
    fontSize: 25,
    fontFamily: 'cabin',
    color: '#555',
    marginBottom: 20
  }
})

class ErrorPage extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <Image source={images.warning} style={styles.messageIcon} />
        <Text style={styles.messageText}>An error occured</Text>
        <TouchableHighlight onPress={this.props.onPress} style={button}>
          <Text style={buttonText}>RETRY</Text>
        </TouchableHighlight>
      </View>
    );
  }

}

export default ErrorPage;
