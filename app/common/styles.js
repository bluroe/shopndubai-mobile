import { create } from './AppStyleSheet';
import { colors } from '../config/styles';
import { Platform } from 'react-native';

export const button = {
  height: 36,
  backgroundColor: colors.primaryBtnBg,
  alignItems: 'center',
  justifyContent: 'center',
  paddingRight: 40,
  paddingLeft: 40,
  borderRadius: Platform.OS == 'ios' ? 18 : 2,
  alignSelf: 'center',
  elevation: 2,
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 0
  },
  shadowRadius: 3,
  shadowOpacity: .3
}

export const buttonText = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'cabin',
  backgroundColor: 'transparent'
}
