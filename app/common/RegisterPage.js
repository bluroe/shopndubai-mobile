import React from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  TextInput,
  ActivityIndicator,
  Alert,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
} from 'react-native';
import images from '../config/images';
import { create } from '../common/AppStyleSheet';
import { colors } from '../config/styles';
import {
  button,
  buttonText
} from './styles';
import axios from 'axios';
import {
  setKey,
  sendOtp,
  requestOtp
} from '../actions/customer';
import settings from '../config/settings';
import dismissKeyboard from 'dismissKeyboard';

const styles = create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    flexDirection: 'row'
  },
  content: {
    flex: 1,
    paddingHorizontal: 40,
  },
  icon: {
    width: 200,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginBottom: 20
  },
  head: {
    fontSize: 24,
    fontFamily: 'cabin',
    textAlign: 'center',
    marginBottom: 5,
    color: '#777'
  },
  text: {
    fontFamily: 'cabin',
    textAlign: 'center',
    color: '#999'
  },
  input: {
    textAlign: 'center',
    marginBottom: 10,
    fontSize: 24,
    color: '#555',
    ios: {
      height: 50,
      borderColor: '#ccc',
      borderRadius: 25,
      borderWidth: 1,
      marginTop: 10
    }
  },
  link: {
    alignSelf: 'center',
    marginBottom: 5
  },
  linkCont: {
    flexDirection: 'row'
  },
  linkText: {
    fontFamily: 'cabin',
    color: '#555',
    marginBottom: 10
  },
  btnCont: {
    marginBottom: 10
  },
  loading: {
    marginLeft: 20
  },
  errorText: {
    color: colors.dangerColor,
    fontFamily: 'cabin',
    marginBottom: 10,
    textAlign: 'center',
  },
  button: {
    backgroundColor: colors.primaryBtnBg,
    alignItems: 'center',
    justifyContent: 'center',
    height: 36,
    paddingHorizontal: 40,
    borderRadius: 18,
    alignSelf: 'center',
    elevation: 2,
    shadowColor: 'rgba(0,0,0,.3)',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 5,
    shadowOpacity: .3,
    marginTop: 10
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'cabin',
  }
});

class OTPPage extends React.Component {

  constructor() {
    super();
    this.state = {
      otp: '',
      msg: '',
      errorMsg: '',
      sending: false,
      submitReady: false,
      otpResending: false
    }
    this.validateOtp = this.validateOtp.bind(this);
  }

  onResendOtp() {
    console.log('resending')
    this.setState({
      otpResending: true,
      // msg: 'Resending OTP...'
    });
    this.props.dispatch(requestOtp(this.props.mobile))
      .then((rsp) => {
        rsp = rsp.data;
        this.setState({
          otpResending: false,
          // msg: 'OTP Resent'
        })
      })
  }

  validateOtp(otp) {
    if(!otp) {
      return false;
    }
    if(otp.length != 4) {
      return false;
    }
    return true;
  }

  onOtpSubmit() {
    const { submitReady, otp } = this.state;
    if(!submitReady) return;
    this.setState({
      sending: true
    });
    this.props.dispatch(sendOtp(otp, this.props.mobile))
      .then((rsp) => {
        console.log(rsp)
        rsp = rsp.data;
        this.setState({
          sending: false
        })
        if(rsp.status == 'ok') {
          this.props.dispatch(setKey(rsp.key));
        } else {
          const msg = typeof rsp.msg === 'string' ? rsp.msg : 'Unknown error';
          Alert.alert(
            'Error',
            msg,
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        }
      });
  }

  onChangeText(otp) {
    console.log(this.validateOtp(otp) ? 'valid' : 'invalid')
    this.setState({
      otp,
      submitReady: this.validateOtp(otp)
    });
  }

  render() {
    const { sending,
      otp,
      errorMsg,
      msg,
      submitReady,
      otpResending
    } = this.state;
    return (
      <KeyboardAvoidingView style={styles.content} behavior="position">
        <Image source={images.telyvert} style={styles.icon} />
        <Text style={styles.head}>Enter OTP</Text>
        <Text style={styles.text}>An OTP has been sent to {this.props.mobile}</Text>
        <TextInput style={styles.input} keyboardType="numeric" onChangeText={this.onChangeText.bind(this)} />
        <View style={styles.btnCont}>
          <TouchableHighlight style={styles.button} onPress={this.onOtpSubmit.bind(this)} underlayColor={colors.primaryBtnBgDark}>
            <View style={styles.linkCont}>
              <Text style={[styles.buttonText, submitReady ? null : {color:'rgba(255,255,255,.3)'}]}>VERIFY NUMBER</Text>
              {sending && <ActivityIndicator size="small" style={styles.loading} />}
            </View>
          </TouchableHighlight>
        </View>
        <Text style={styles.errorText}>{errorMsg}</Text>
        <TouchableOpacity style={styles.link} onPress={this.onResendOtp.bind(this)}>
          <View style={styles.linkCont}>
            <Text style={styles.linkText}>{'Didn\'t Receive OTP?'}</Text>
            {otpResending && <ActivityIndicator size="small" style={styles.loading} />}
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.link} onPress={this.props.changeNumber}>
          <Text style={styles.linkText}>Change Number</Text>
        </TouchableOpacity>
        <Text style={{textAlign:'center'}}>{msg}</Text>
      </KeyboardAvoidingView>
    )
  }
}


class RegisterPage extends React.Component {

  constructor() {
    super();
    this.state = {
      waiting: false,
      sending: false,
      mobile: '',
      error: '',
    }
    this.validateNumber = this.validateNumber.bind(this);
  }

  validateNumber() {
    const { mobile } = this.state;
    if(!mobile) {
      this.setState({
        error: 'Please enter mobile number'
      });
      return false;
    }
    if(mobile.length != 10) {
      this.setState({
        error: 'Number should be 10 digit long'
      });
      return false;
    }
    return true;
  }

  onMobileSubmit() {
    if(this.state.sending) return;
    const valid = this.validateNumber();
    if(!valid) return;
    this.setState({
      sending: true,
      error: ''
    });
    const { mobile } = this.state;
    this.props.dispatch(requestOtp(mobile))
      .then((rsp) => {
        rsp = rsp.data;
        console.log(rsp)
        if(rsp.status === 'ok') {
          this.setState({
            waiting: true,
            sending: false
          })
        } else {
          this.setState({
            sending: false
          })
          const msg = typeof rsp.msg === 'string' ? rsp.msg : 'Unknown error';
          Alert.alert(
            'Error',
            msg,
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        }
      })
  }

  comeBack() {
    this.setState({waiting:false})
  }

  render() {
    const { waiting, sending, error, mobile } = this.state;

    let content = null;
    if(waiting) {
      content = (
        <OTPPage
          mobile={mobile}
          changeNumber={this.comeBack.bind(this)}
          dispatch={this.props.dispatch}
        />
      );
    } else {
      content = (
        <KeyboardAvoidingView style={styles.content} behavior="position">
          <Image source={images.telyvert} style={styles.icon} />
          <Text style={styles.head}>Register your number</Text>
          <Text style={styles.text}>An SMS with a one time password will be sent to this number</Text>
          <TextInput style={styles.input} keyboardType="numeric" onChangeText={(mobile) => this.setState({mobile})} />
          <Text style={styles.errorText}>{error}</Text>
          <TouchableHighlight style={styles.button} onPress={this.onMobileSubmit.bind(this)} underlayColor={colors.primaryBtnBgDark}>
            <View style={styles.linkCont}>
              <Text style={[styles.buttonText, sending ? {color:'rgba(255,255,255,.3)'} : null]}>RECEIVE OTP</Text>
              {sending && <ActivityIndicator size="small" style={styles.loading} />}
            </View>
          </TouchableHighlight>
        </KeyboardAvoidingView>
      )
    }

    return (
      <TouchableWithoutFeedback onPress={dismissKeyboard}>
        <View style={styles.container}>
          {content}
        </View>
      </TouchableWithoutFeedback>
    )
  }

}

export default connect()(RegisterPage);
