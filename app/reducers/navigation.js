const initialState = {
  tab: 'home'
}

export default function location (state = initialState, action) {
  switch (action.type) {
    case 'SWITCH_TAB':
      return {
        ...state,
        tab: action.tab
      }
    default:
      return state;
  }
}
