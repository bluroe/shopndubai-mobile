const initialState = [];

export default function orders(state = initialState, action) {
  switch(action.type) {
    case 'SET_ORDERS':
      return {
        ...state,
        orders: action.orders
      }
    default:
      return state;
  }
}
