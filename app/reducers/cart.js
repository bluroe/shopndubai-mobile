const initialState = {
  items: [],
  deliveryCharge: 0
}

export default function cart (state = initialState, action) {
  switch(action.type) {
    case 'ADD_ITEM':
      const {
        name,
        images
      } = action.product;
      let variant = action.product.variants[0];
      if(action.variant) {
        variant = action.product.variants.filter((v) => v.id == action.variant)[0];
      }
      const image = images.length ? images[0].filename : null;
      const {
        id,
        price,
        type
      } = variant;
      const qty = action.qty || 1;
      const outgoing = {
        id,
        name,
        image,
        price,
        qty,
        type
      };
      var product = state.items.filter(item => item.id == outgoing.id)[0];
      let items = null;
      if(product) {
        items = state.items.map(item => {
          if(item.id === product.id) {
            var _item = {
              ...item,
              qty: action.qty || product.qty + 1
            }
            return _item;
          }
          return item;
        });
      } else {
        items = [
          ...state.items,
          outgoing
        ];
      }
      return {
        ...state,
        items
      }
    case 'REMOVE_ITEM':
      return {
        ...state,
        items: state.items.filter(item => item.id != action.id)
      }
    case 'REMOVE_ITEMS':
      return {
        ...state,
        items: state.items.filter(item => action.items.indexOf(item.id) == -1)
      }
    case 'CLEAR':
      // console.log('clearing')
      return {
        ...state,
        items: []
      }
    case 'SET_DELIVERY_CHARGE':
      return {
        ...state,
        deliveryCharge: action.amount
      }
    default:
      return state;
  }
}
