const initialState = {
  pin: '',
  stores: 0,
  active: false
}

export default function location (state = initialState, action) {
  switch (action.type) {
    case 'SET_PIN':
      return {
        ...state,
        pin: action.pin
      }
    case 'SET_STORES':
      return {
        ...state,
        stores: action.stores
      }
    case 'SET_ACTIVE':
      return {
        ...state,
        active: action.active
      }
    default:
      return state;
  }
}
