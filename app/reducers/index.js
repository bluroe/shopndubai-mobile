import { combineReducers } from 'redux';

import customer from './customer';
import location from './location';
import cart from './cart';
import navigation from './navigation';
import orders from './orders';

export default combineReducers({
  customer, location, cart, navigation, orders
});
