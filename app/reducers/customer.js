const initialState = {
  key: '',
  addresses: []
}

export default function customer (state = initialState, action) {
  switch (action.type) {
    case 'SET_KEY':
      return {
        ...state,
        key: action.key
      }
    case 'SET_KEY_ERROR':
      return {
        ...state
      }
    case 'ADD_ADDRESS':
      return {
        ...state,
        addresses: [
          ...state.addresses,
          action.address
        ]
      }
    case 'EDIT_ADDRESS':
      return {
        ...state,
        addresses: [
          ...state.addresses.slice(0, action.index),
          action.address,
          ...state.addresses.slice(action.index + 1)
        ]
      }
    case 'REMOVE_ADDRESS':
      return {
        ...state,
        addresses: [
          ...state.addresses.slice(0, action.index),
          ...state.addresses.slice(action.index + 1)
        ]
      }
    default:
      return state;
  }
}
