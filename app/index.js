import React, { Component } from 'react';
import { Provider } from 'react-redux';
import App from './App';
import location from './reducers/location';
import makeStore from './config/store';

export default class AwesomeProject extends Component {

  constructor() {
    super();
    // const _XHR = GLOBAL.originalXMLHttpRequest ?
    //     GLOBAL.originalXMLHttpRequest :
    //     GLOBAL.XMLHttpRequest
    // XMLHttpRequest = _XHR

    this.state = {
      isLoading: true,
      store: makeStore(() => this.setState({isLoading: false})),
    };
  }

  render() {
    if(this.state.isLoading) {
      return null;
    }
    return (
      <Provider store={this.state.store}>
        <App />
      </Provider>
    );
  }

}
